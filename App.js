import React from "react";

import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import Register from "./screens/Register/Register";

import Information from "./screens/Information/Information";
import { Restaurant, OrderDelivery, CompleteOrderDelivery } from "./screens";
import CompleteFood from "./screens/CompleteFood";
import Bank from "./screens/BankScreen/index";
import chooseBank from "./screens/BankScreen/chooseBank";
import AccountBankScreen from "./screens/BankScreen/accountBank";
import InfoBankScreen from "./screens/BankScreen/InfoBank";
import Tabs from "./navigation/tabs";
const store = createStore(reducers, {}, applyMiddleware(reduxThunk));
import reducers from "./src/reducers";
import { Provider } from "react-redux";
import HistoryScreen from "./screens/HistoryScreen/HistoryScreen";

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
          initialRouteName={"Home"}
        >
          <Stack.Screen name="Home" component={Tabs} />
          <Stack.Screen name="Restaurant" component={Restaurant} />
          <Stack.Screen name="Bank" component={Bank} />
          <Stack.Screen name="chooseBank" component={chooseBank} />
          <Stack.Screen
            name="AccountBankScreen"
            component={AccountBankScreen}
          />
          <Stack.Screen name="InfoBankScreen" component={InfoBankScreen} />
          <Stack.Screen name="OrderDelivery" component={OrderDelivery} />
          <Stack.Screen
            name="CompleteOrderDelivery"
            component={CompleteOrderDelivery}
          />
          <Stack.Screen name="CompleteFood" component={CompleteFood} />
          <Stack.Screen name="HistoryScreen" component={HistoryScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
