import { icons, images, SIZES, COLORS, FONTS } from "../constants";
export default (categoryData = [
  {
    id: 1,
    name: "Cơm",
    icon: icons.rice_bowl,
  },
  {
    id: 2,
    name: "Mì",
    icon: icons.noodle,
  },
  {
    id: 3,
    name: "Hot Dogs",
    icon: icons.hotdog,
  },
  {
    id: 4,
    name: "Salads",
    icon: icons.salad,
  },
  {
    id: 5,
    name: "Burgers",
    icon: icons.hamburger,
  },
  {
    id: 6,
    name: "Pizza",
    icon: icons.pizza,
  },
  {
    id: 7,
    name: "Snacks",
    icon: icons.fries,
  },
  {
    id: 8,
    name: "Sushi",
    icon: icons.sushi,
  },
  {
    id: 9,
    name: "Desserts",
    icon: icons.donut,
  },
  {
    id: 10,
    name: "Nước",
    icon: icons.drink,
  },
]);
