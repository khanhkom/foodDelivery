export default (disCode = [
  {
    id: 1,
    Title: "XINCHAO", // Mã giảm giá
    payments: null, // hình thức thanh toán ,
    content: "Giảm 60% tiền ship áp dụng cho đơn hàng từ 35K ", // nội dung
    Condition: "35000", // Điều kiện (số tiền tối thiểu)
    value: "60", // giá trị của mã
    percent: true, // or false,( tru khi giảm theo phần trăm, false khi trả bằng tiền mặt)
    ExpirationDate: "31-12-2021", // ngày hết hạn sử dụng
    shipMoney: true, // or false, (nếu true thì giảm tiền ship)
  },
  {
    id: 2,
    Title: "XINCHAO1", // Mã giảm giá
    payments: null, // hình thức thanh toán ,
    content: "Giảm 60% áp dụng cho mọi đơn hàng ", // nội dung
    Condition: null, // Điều kiện (số tiền tối thiểu)
    value: "60", // giá trị của mã
    percent: true, // or false,( tru khi giảm theo phần trăm, false khi trả bằng tiền mặt)
    ExpirationDate: "31-12-2021", // ngày hết hạn sử dụng
    shipMoney: false, // or false, (nếu true thì giảm tiền ship)
  },
  {
    id: 3,
    Title: "XINCHAO2", // Mã giảm giá
    payments: "0", // hình thức thanh toán ,
    content: "Giảm 60% áp dụng cho đơn hàng trả bằng tiền mặt ", // nội dung
    Condition: null, // Điều kiện (số tiền tối thiểu)
    value: "60", // giá trị của mã
    percent: true, // or false,( tru khi giảm theo phần trăm, false khi trả bằng tiền mặt)
    ExpirationDate: "31-12-2021", // ngày hết hạn sử dụng
    shipMoney: false, // or false, (nếu true thì giảm tiền ship)
  },
  {
    id: 4,
    Title: "XINCHAO3", // Mã giảm giá
    payments: null, // hình thức thanh toán ,
    content: "Giảm 20K áp dụng cho đơn hàng từ 35K ", // nội dung
    Condition: "35000", // Điều kiện (số tiền tối thiểu)
    value: "2000", // giá trị của mã
    percent: false, // or false,( tru khi giảm theo phần trăm, false khi trả bằng tiền mặt)
    ExpirationDate: "31-12-2021", // ngày hết hạn sử dụng
    shipMoney: false, // or false, (nếu true thì giảm tiền ship)
  },
  {
    id: 5,
    Title: "XINCHAO4", // Mã giảm giá
    payments: "1", // hình thức thanh toán ,
    content: "Giảm 20K áp dụng cho đơn hàng trả bằng thẻ ", // nội dung
    Condition: null, // Điều kiện (số tiền tối thiểu)
    value: "20000", // giá trị của mã
    percent: false, // or false,( tru khi giảm theo phần trăm, false khi trả bằng tiền mặt)
    ExpirationDate: "31-12-2021", // ngày hết hạn sử dụng
    shipMoney: true, // or false, (nếu true thì giảm tiền ship)
  },
  {
    id: 6,
    Title: "XINCHAO5", // Mã giảm giá
    payments: "1", // hình thức thanh toán ,
    content: "Giảm 60% áp dụng cho đơn hàng trả bằng thẻ ", // nội dung
    Condition: null, // Điều kiện (số tiền tối thiểu)
    value: "60", // giá trị của mã
    percent: true, // or false,( tru khi giảm theo phần trăm, false khi trả bằng tiền mặt)
    ExpirationDate: "31-12-2021", // ngày hết hạn sử dụng
    shipMoney: false, // or false, (nếu true thì giảm tiền ship)
  },
  {
    id: 7,
    Title: "XINCHAO6", // Mã giảm giá
    payments: null, // hình thức thanh toán ,
    content: "Freeship cho mọi đơn hàng ", // nội dung
    Condition: null, // Điều kiện (số tiền tối thiểu)
    value: "100", // giá trị của mã
    percent: true, // or false,( tru khi giảm theo phần trăm, false khi trả bằng tiền mặt)
    ExpirationDate: "31-12-2021", // ngày hết hạn sử dụng
    shipMoney: true, // or false, (nếu true thì giảm tiền ship)
  },
]);
