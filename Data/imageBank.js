import { icons, images, SIZES, COLORS, FONTS, imageb } from "../constants";
export default (imageBank = [
  {
    id: 1,
    name: "ABCbank",
    image: imageb.ABCbank,
  },
  {
    id: 2,
    name: "VPbank",
    image: imageb.VPbank,
  },
  {
    id: 3,
    name: "Viettinbank",
    image: imageb.Viettinbank,
  },
  {
    id: 4,
    name: "Techcombank",
    image: imageb.Techcombank,
  },
  {
    id: 5,
    name: "Sacombank",
    image: imageb.Sacombank,
  },
  {
    id: 6,
    name: "Visa",
    image: imageb.Visa,
  },
  {
    id: 7,
    name: "Agribank",
    image: imageb.Agribank,
  },
  {
    id: 8,
    name: "MBbank",
    image: imageb.MBbank,
  },
  {
    id: 9,
    name: "JCB",
    image: imageb.JCB,
  },
  {
    id: 10,
    name: "Mastercard",
    image: imageb.Mastercard,
  },
]);
