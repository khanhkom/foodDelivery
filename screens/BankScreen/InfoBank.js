import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  ScrollView,
  TextInput,
  Keyboard,
} from "react-native";

import { icons, images, SIZES, COLORS } from "../../constants";
const { width, height } = Dimensions.get("window");
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import LinearGradient from "react-native-linear-gradient";
import Fontisto from "react-native-vector-icons/Fontisto";
// import imageBank from "../../Data/imageBank";
import auth from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
import { useDispatch, useSelector } from "react-redux";
import { setListBankConnected } from "../../src/actions/bank";
const InfoBank = ({ route, navigation }) => {
  const {bankInfor} = route?.params;
  console.log('bankInfor_bankInfor',bankInfor)
  const [textinput, setTextinput] = useState("");
  const [nameBank, setNameBank] = useState("");
  useEffect(() => {
    setNameBank("abc");
  }, []);
  const dispatch = useDispatch()
  const bank = useSelector((state) => state.bank);
  const onDelBank =async ()=>{
    let bankParse = JSON.parse(JSON.stringify(bank))
    let newBank = bankParse?.filter((element)=>{
      return element?.idne!== bankInfor?.idne
    })
    const res = await firestore().collection('bank').doc(bankInfor?.idne).delete();

    dispatch(setListBankConnected(newBank))
    console.log('newBank_newBank',res,newBank)
    navigation.goBack()

  }
  function renderHeader() {
    return (
      <View style={{ flexDirection: "row", height: 50, marginTop: 24 }}>
        <TouchableOpacity
          style={{
            width: 50,
            paddingLeft: SIZES.padding * 2,
            justifyContent: "center",
          }}
          onPress={() => navigation.goBack()}
        >
          <Image
            source={icons.back}
            resizeMode="contain"
            style={{
              width: 30,
              height: 30,
            }}
          />
        </TouchableOpacity>

        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              width: "70%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: SIZES.radius,
              padding: 16,
            }}
          >
            <Text
              style={{ fontSize: 18, textAlign: "auto", fontWeight: "bold" }}
              numberOfLines={2}
            >
              Thông tin thẻ
            </Text>
          </View>
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}
    <ScrollView>

      <View style={{ margin: 16 }}>
        <LinearGradient
          colors={["#1db853", "#01ab9d", "#FF66CC"]}
          style={{
            width: width - 32,
            paddingVertical: 16,
            // height: 300,
            // backgroundColor: "black",
            borderRadius: 30,
            borderWidth: 1,
          }}
        >
          <View
            style={{
              width: width - 32,
              justifyContent: "space-between",
              paddingHorizontal: 32,
              alignItems: "center",
              flexDirection: "row",
              marginVertical: 10,
            }}
          >
            <Image source={images.iconApp} style={{ height: 60, width: 60 }} />
            <Image source={images.iconBank} style={{ height: 50, width: 50 }} />
          </View>

          <View
            style={{
              width: width - 32,
              justifyContent: "space-between",
              paddingHorizontal: 32,
              alignItems: "center",
              flexDirection: "row",
              marginVertical: 10,
            }}
          >
            <Text style={{ fontSize: 16, color: "white", fontWeight: "bold" }}>
              Ngân hàng
            </Text>
            <Text style={{ fontSize: 16, color: "white", fontWeight: "bold" }}>
            {bankInfor?.bankname}
            </Text>
          </View>
          <View
            style={{
              width: width - 32,
              justifyContent: "space-between",
              paddingHorizontal: 32,
              alignItems: "center",
              flexDirection: "row",
              marginVertical: 10,
            }}
          >
            <Text style={{ fontSize: 16, color: "white", fontWeight: "bold" }}>
              Số tài khoản
            </Text>
            <Text style={{ fontSize: 16, color: "white", fontWeight: "bold" }}>
              {bankInfor?.banknumber}
            </Text>
          </View>
          <View
            style={{
              width: width - 32,
              justifyContent: "space-between",
              paddingHorizontal: 32,
              alignItems: "center",
              flexDirection: "row",
              marginVertical: 10,
            }}
          >
            <Text style={{ fontSize: 16, color: "white", fontWeight: "bold" }}>
              Trạng thái
            </Text>
            <Text style={{ fontSize: 16, color: "white", fontWeight: "bold" }}>
              Đang liên kết
            </Text>
          </View>
        </LinearGradient>

        <View style={styles.button}>
          <TouchableOpacity onPress={onDelBank} style={styles.signIn}>
            <LinearGradient
              colors={["#1db853", "#01ab9d"]}
              style={styles.signIn}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: "#fff",
                  },
                ]}
              >
                Hủy liên kết
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 0.5,
            width: width - 64,
            backgroundColor: "black",
            marginVertical: 6,
            marginLeft: 16,
          }}
        />
        <View>
          <Text style={{ fontSize: 16, color: "black", marginVertical: 6 }}>
            LỢI ÍCH KHI LIÊN KẾT NGÂN HÀNG
          </Text>
          <Text style={{ fontSize: 16, color: "black" }}>
            1. Nhiều tiện ích
          </Text>
          <Text style={{ fontSize: 16, color: "black", marginVertical: 6 }}>
            - Thanh toán nhanh chóng hàng trăm dịch vụ...
          </Text>
          <Text style={{ fontSize: 16, color: "black" }}>
            2. Siêu tiết kiệm
          </Text>
          <Text style={{ fontSize: 16, color: "black", marginVertical: 6 }}>
            - Không tốn phí thường niên.
          </Text>
          <Text style={{ fontSize: 16, color: "black" }}>
            - Thường xuyên ưu đãi ăn uống.
          </Text>
          <Text style={{ fontSize: 16, color: "black", marginVertical: 6 }}>
            3. An toàn tuyệt đối
          </Text>
          <Text style={{ fontSize: 16, color: "black" }}>
            - Bảo mật đa tầng chuẩn quốc tế, không lưu thông tin thẻ.
          </Text>
        </View>
      </View>
    </ScrollView>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1,
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 12,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});

export default InfoBank;
