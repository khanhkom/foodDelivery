import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  ScrollView,
  TextInput,
  Keyboard,
  Alert,
} from "react-native";

import {
  icons,
  images,
  SIZES,
  COLORS,
  FONTS,
  GOOGLE_API_KEY,
  imageb,
} from "../../constants";
const { width, height } = Dimensions.get("window");
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import LinearGradient from "react-native-linear-gradient";
import Fontisto from "react-native-vector-icons/Fontisto";
// import imageBank from "../../Data/imageBank";
import auth from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
import { useDispatch, useSelector } from "react-redux";
import { setListBankConnected } from "../../src/actions/bank";

const accountBank = ({ route, navigation }) => {
  const [textinput, setTextinput] = useState("");
  const [nameBank, setNameBank] = useState("");
  const [bankuser, setBankUser] = useState("");
  const [banknumber, setBanknumber] = useState("");
  const [exprieddate, setEprieddate] = useState("");
  const [status, setStatus] = useState("");
  const bank = useSelector((state) => state.bank);
  const dispatch = useDispatch();

  useEffect(() => {
    let { namebank } = route.params;
    setNameBank(namebank);
  }, []);
  const onsubmit = async () => {
    const user = auth().currentUser;
    let { bankinfor } = route.params;

    // console.log('user_user',user?.email)
    let body = {
      bankname: nameBank,
      bankuser: bankuser,
      banknumber: banknumber,
      exprieddate: exprieddate,
      status: 1,
      email: user?.email,
      bankinfor: bankinfor,
    };
    let currentBank = JSON.parse(JSON.stringify(bank));
    console.log("currentBank_currentBank", currentBank);
    console.log("currentBank_currentBank", [...currentBank, body]);
    console.log("body", body);
    if (bankuser === "" || banknumber === "" || exprieddate === "") {
      Alert.alert("Thông báo", "Vui lòng nhập đầy đủ thông tin", ["Ok"]);
    } else {
      await firestore()
        .collection("bank")
        .add(body);
      dispatch(setListBankConnected([...currentBank, body]));
      Alert.alert("Thông báo", "Thêm tài khoản ngân hàng thành công", [
        {
          text: "OK",
          onPress: () => navigation.navigate("Bank"),
          style: "OK",
        },
      ]);
    }
    console.log("body_body", body);
  };
  function renderHeader() {
    return (
      <View style={{ flexDirection: "row", height: 50, marginTop: 24 }}>
        <TouchableOpacity
          style={{
            width: 50,
            paddingLeft: SIZES.padding * 2,
            justifyContent: "center",
          }}
          onPress={() => navigation.goBack()}
        >
          <Image
            source={icons.back}
            resizeMode="contain"
            style={{
              width: 30,
              height: 30,
            }}
          />
        </TouchableOpacity>

        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              width: "70%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: SIZES.radius,
              padding: 16,
            }}
          >
            <Text
              style={{ fontSize: 18, textAlign: "auto", fontWeight: "bold" }}
              numberOfLines={2}
            >
              {nameBank}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Text style={{ margin: 16, color: "black", fontSize: 20 }}>
          THÔNG TIN THẺ TÍN DỤNG
        </Text>
        <View
          style={{ height: 1, width: width - 64, backgroundColor: "black" }}
        />
      </View>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{ margin: 16 }}
      >
        <Text style={{ margin: 16, fontSize: 18, fontWeight: "bold" }}>
          Thông tin giao dịch
        </Text>
        <View
          style={{
            width: width - 32,
            paddingVertical: 16,
            // height: 300,
            // backgroundColor: "black",
            borderRadius: 30,
            borderWidth: 1,
          }}
        >
          <View
            style={{
              width: width - 32,
              justifyContent: "space-between",
              paddingHorizontal: 32,
              alignItems: "center",
              flexDirection: "row",
              marginVertical: 10,
            }}
          >
            <Text style={{ fontSize: 16 }}>Nguồn tiền :</Text>
            <Text style={{ fontSize: 16 }}>{nameBank}</Text>
          </View>
          <View
            style={{
              width: width - 32,
              justifyContent: "space-between",
              paddingHorizontal: 32,
              alignItems: "center",
              flexDirection: "row",
              marginVertical: 10,
            }}
          >
            <Text style={{ fontSize: 16 }}>Số tiền tối thiểu :</Text>
            <Text style={{ fontSize: 16 }}>50.000 đ</Text>
          </View>
          <View
            style={{
              height: 0.5,
              backgroundColor: "black",
              width: width - 64,
              marginLeft: 16,
            }}
          />
          <View
            style={{
              width: width - 32,
              justifyContent: "space-between",
              paddingHorizontal: 32,
              alignItems: "center",
              flexDirection: "row",
              marginVertical: 10,
            }}
          >
            <Text style={{ fontSize: 16 }}>Phí giao dịch :</Text>
            <Text style={{ fontSize: 16 }}>Miến phí</Text>
          </View>
          <View
            style={{
              height: 0.5,
              backgroundColor: "black",
              width: width - 64,
              marginLeft: 16,
            }}
          />
          <View
            style={{
              width: width - 32,
              justifyContent: "space-between",
              paddingHorizontal: 32,
              alignItems: "center",
              flexDirection: "row",
              marginVertical: 10,
            }}
          >
            <Text style={{ fontSize: 16 }}>Tổng tiền giao dịch :</Text>
            <Text style={{ fontSize: 16 }}>Miến phí</Text>
          </View>
          <Text
            style={{
              marginLeft: 32,
              marginRight: 16,
              fontSize: 16,
              fontWeight: "bold",
            }}
          >
            Để đảm bảo thẻ còn hoạt động, hệ thống yêu cầu số tiền tối thiểu
            trong thẻ là 50.000đ
          </Text>
        </View>
        <Text
          style={[
            styles.text_footer,
            {
              color: "#000",
              marginTop: 12,
            },
          ]}
        >
          Chủ tài khoản
        </Text>
        <View style={styles.action}>
          <FontAwesome name="user-o" color={"#000"} size={20} />
          <TextInput
            placeholder="Chủ tài khoản"
            onChangeText={setBankUser}
            placeholderTextColor="#666666"
            style={[
              styles.textInput,
              {
                color: "#000",
              },
            ]}
            autoCapitalize="none"
            // onChangeText={(val) => textInputChange(val)}
            // onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
          />
        </View>

        <Text
          style={[
            styles.text_footer,
            {
              color: "#000",
              marginTop: 12,
            },
          ]}
        >
          Ngày hết hạn
        </Text>
        <View style={styles.action}>
          <Fontisto name="date" color={"#000"} size={20} />
          <TextInput
            placeholder="Ngày hết hạn"
            onChangeText={setEprieddate}
            placeholderTextColor="#666666"
            style={[
              styles.textInput,
              {
                color: "#000",
              },
            ]}
            autoCapitalize="none"
            // onChangeText={(val) => textInputChange(val)}
            // onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
          />
        </View>

        <Text
          style={[
            styles.text_footer,
            {
              color: "#000",
              marginTop: 12,
            },
          ]}
        >
          Số thẻ
        </Text>
        <View style={styles.action}>
          <FontAwesome name="credit-card" color={"#000"} size={20} />
          <TextInput
            placeholder="Số thẻ"
            keyboardType="numeric"
            onChangeText={setBanknumber}
            placeholderTextColor="#666666"
            style={[
              styles.textInput,
              {
                color: "#000",
              },
            ]}
            autoCapitalize="none"
            // onChangeText={(val) => textInputChange(val)}
            // onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
          />
        </View>

        <View style={styles.button}>
          <TouchableOpacity onPress={onsubmit} style={styles.signIn}>
            <LinearGradient
              colors={["#1db853", "#01ab9d"]}
              style={styles.signIn}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: "#fff",
                  },
                ]}
              >
                Liên kết
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1,
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});

export default accountBank;
