import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  ScrollView,
  TextInput,
  Keyboard,
} from "react-native";

import {
  icons,
  images,
  SIZES,
  COLORS,
  FONTS,
  GOOGLE_API_KEY,
  imageb,
} from "../../constants";
const { width, height } = Dimensions.get("window");
import FontAwesome from "react-native-vector-icons/FontAwesome";
import imageBank from "../../Data/imageBank";

const BankScreen = ({ navigation }) => {
  const [textinput, setTextinput] = useState("");
  const [nameBank, setNameBank] = useState(imageBank);
  useEffect(() => {
    setNameBank(imageBank);
    console.log(imageBank);
  }, []);
  function renderHeader() {
    return (
      <View style={{ flexDirection: "row", height: 50, marginTop: 24 }}>
        <TouchableOpacity
          style={{
            width: 50,
            paddingLeft: SIZES.padding * 2,
            justifyContent: "center",
          }}
          onPress={() => navigation.goBack()}
        >
          <Image
            source={icons.back}
            resizeMode="contain"
            style={{
              width: 30,
              height: 30,
            }}
          />
        </TouchableOpacity>

        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              width: "70%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: SIZES.radius,
              padding: 16,
            }}
          >
            <Text
              style={{ fontSize: 18, textAlign: "auto", fontWeight: "bold" }}
              numberOfLines={2}
            >
              Chọn ngân hàng
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderchooseBanK() {
    return (
      <View style={{ flexDirection: "row", height: 50 }}>
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              width: "90%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "space-around",
              borderRadius: SIZES.radius,
              flexDirection: "row",
            }}
          >
            <FontAwesome name="search" color="black" size={24} />

            <TextInput
              style={{
                ...FONTS.h3,
                flex: 7 / 10,
                justifyContent: "center",
                // backgroundColor: 'white',
                color: "black",
                fontSize: 16,
              }}
              placeholder="Nhập ngân hàng..."
              placeholderTextColor="gray"
              onChangeText={(value) => setTextinput(value)}
              onEndEditing={() => {
                navigation.navigate("AccountBankScreen", {
                  namebank: textinput,
                });
                Keyboard.dismiss();
              }}
            />
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("AccountBankScreen", {
                  namebank: textinput,
                });
                Keyboard.dismiss();
              }}
            >
              <Text style={{ fontSize: 16, color: "black" }}>Nhập</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  const rendernameBank = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate("AccountBankScreen", { namebank: item.item.name,bankinfor:item })
        }
        style={{
          marginLeft: 0,
          justifyContent: "center",
          alignItems: "center",
          width: width / 3,
          marginBottom: 12,
        }}
      >
        <Image
          source={item.item.image}
          resizeMode="contain"
          style={{
            width: 40,
            height: 40,
          }}
        />
        <Text>{item.item.name}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}

      <View
        style={{
          margin: 16,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <FontAwesome name="group" color={COLORS.primary} size={24} />
        <Text style={{ fontSize: 11, marginHorizontal: 16 }}>
          Hơn 11 triệu người đã tin tưởng lưu thẻ/ tài khoản tại Fooddelivery
        </Text>
      </View>
      {/* nhập tên ngân hàng*/}
      <View style={{ margin: 16 }}>
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>
          Nhập tên ngân hàng
        </Text>
      </View>
      {renderchooseBanK()}

      <View>
        <Text style={{ margin: 16 }}>Gợi ý một số ngân hàng</Text>

        <ScrollView>
          <FlatList
            numColumns={3}
            data={nameBank}
            renderItem={(item) => rendernameBank(item)}
          />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1,
  },
});

export default BankScreen;
