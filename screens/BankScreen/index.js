import React, { useEffect,useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  ScrollView,
} from "react-native";

import {
  icons,
  images,
  SIZES,
  COLORS,
  FONTS,
  GOOGLE_API_KEY,
} from "../../constants";
const { width, height } = Dimensions.get("window");
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { useDispatch, useSelector } from "react-redux";
import firestore from "@react-native-firebase/firestore";
import {setListBankConnected} from '../../src/actions/bank';
import auth from "@react-native-firebase/auth";
import AsyncStorage from "@react-native-community/async-storage";

const BankScreen = ({ navigation }) => {
  const user = useSelector(state=>state.user)
  const [userInformation,setUserInfor] = useState({

  })
  const bank = useSelector((state) => state.bank);

  const dispatch = useDispatch()
  useEffect(()=>{
    async function getUser(){
      let Account = await AsyncStorage.getItem("@Account");
      console.log('Account',Account)
      if(Account&&Account!==null){
        setUserInfor(JSON.parse(Account))
        }else{
      setUserInfor({})
        }
    }
    getUser()
  },[user])
  useEffect(() => {
    const users = auth().currentUser;
    console.log('user_user',user?.email)
    
    const getBankByUser = async (user) => {
      try {
          const banks = await firestore()
          .collection("bank")
          .get();
          console.log('banks_banks',banks.docs)
        let allBank = banks.docs.map((doc) => {
          return {
            idne: doc.id,
            ...doc.data(),
          };
        });
        console.log('allBank_allBank',allBank)

        let currentBank = allBank.filter((element) => {
          return element.email === user?.email;
        });
        if (currentBank && currentBank !== null) {
          dispatch(setListBankConnected(currentBank))
        }
      console.log('currentBank_currentBank',allBank,currentBank);
      } catch (error) {
          console.log('error_error',error)
      }
    // console.log('user_user',user);
  }
  getBankByUser(users)
  }, [user]);
  console.log('userInformation_userInformation',userInformation)
  function renderHeader() {
    return (
      <View style={{ flexDirection: "row", height: 50, marginTop: 24 }}>
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={styles.header}
          >
            <Text
              style={{ fontSize: 18, textAlign: "auto", fontWeight: "bold" }}
              numberOfLines={2}
            >
              Thêm thẻ/ tài khoản
            </Text>
          </View>
        </View>
      </View>
    );
  }
  const ItemBankConnected = ({item})=>{
    return(
      
      <View>
        <View
            style={[styles.itemInfor,{marginTop:16}]}
        >
          <TouchableOpacity
            onPress={() => navigation.navigate("InfoBankScreen",{bankInfor:item})}
            style={{
              width: width - 40,
              flexDirection: "row",
              paddingHorizontal: 16,
            }}
          >
            <Image source={images.iconBank} style={{ height: 40, width: 40 }} />
            <View style={{ marginLeft: 16 }}>
              <Text>{item?.bankname??''}</Text>
              <Text style={{ color: "gray" }}>{item?.banknumber??''}</Text>
            </View>
          </TouchableOpacity>
          <View>
            <FontAwesome name="angle-right" color="gray" size={24} />
          </View>
        </View>
      </View>
    )
  }
  if(!userInformation?.username){
return(
  <SafeAreaView style={styles.container}>
  {renderHeader()}
  <View style={{ margin: 20,marginTop:40 }}>
    <Text style={{ fontSize: 14, fontWeight: "normal",textAlign:'center' }}>Vui lòng đăng nhập để xem thông tin ngân hàng liên kết</Text>
  </View>
  </SafeAreaView>
)
  }
  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}
      <View style={{ margin: 16 }}>
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>TÀI KHOẢN</Text>
      </View>

      {/* hien danh sach cac ngan hang da ket noi. */}
      {
        bank?.map((item,index)=>{
          return(
            <ItemBankConnected item={item} index={index} key={index}/>
          )
        })
      }
      <View>
        <TouchableOpacity
          onPress={() => navigation.navigate("chooseBank")}
          style={[styles.itemInfor,{marginVertical:16}]}
        >
          <View
            style={styles.boxIcon}
          >
            <FontAwesome name="plus" color="gray" size={24} />
          </View>

          <Text style={{ marginHorizontal: 16, fontSize: 16 }}>
            {" "}
            Thêm thẻ/ tài khoản
          </Text>
        </TouchableOpacity>
        <View>
          <View
            style={styles.itemInfor}
          >
            <View
              style={styles.boxIcon}
            >
              <FontAwesome name="key" color="gray" size={24} />
            </View>

            <Text style={{ marginRight: 100, fontSize: 14 }} numberOfLines={2}>
              FoodDelivery đã được cấp chứng chỉ bảo mật PCI-DSS, hoàn toàn
              không lưu thông tin để ăn cắp dữ liệu người dùng
            </Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray4,
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1,
  },
  boxIcon:{
    height: 40,
    width: 40,
    borderWidth: 0.5,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 16,
    bottom: 0,
  },
  itemInfor:{
    height: 60,
    width: width,
    backgroundColor: "white",
    flexDirection: "row",
    // justifyContent: "space-around",
    alignItems: "center",
  },
  header:{
    width: "70%",
    height: "100%",
    backgroundColor: COLORS.lightGray3,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: SIZES.radius,
    padding: 16,
  }
});

export default BankScreen;
