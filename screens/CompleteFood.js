import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Platform,
  Linking,
  Alert,
  BackHandler,
} from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import Geolocation from "@react-native-community/geolocation";

import { COLORS, FONTS, icons, SIZES, GOOGLE_API_KEY } from "../constants";
import auth from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
import { useDispatch, useSelector } from "react-redux";
import { setDishHistoryOrder } from "../src/actions/dish";
import moment from 'moment'
const CompleteFood = ({ route, navigation }) => {
  const mapView = React.useRef();

  const [restaurant, setRestaurant] = React.useState(null);
  const [currentLocation, setCurrentLocation] = React.useState(null);
  const [streetName, setStreetName] = React.useState("");
  const [fromLocation, setFromLocation] = React.useState(null);
  const [toLocation, setToLocation] = React.useState(null);
  const [region, setRegion] = React.useState(null);

  const [duration, setDuration] = React.useState(0);
  const [isReady, setIsReady] = React.useState(false);
  const [angle, setAngle] = React.useState(0);
  const [time, setTime] = useState(0);
  const [temporaryMoney, setTemporaryMoney] = React.useState(0);
  const [orderItems, setOrderItems] = React.useState(null);

  const [distance, setDistance] = useState(0);
  const [caner, setCaner] = useState(true);
  const history = useSelector((state) => state.history);
  const dispatch = useDispatch();

  React.useEffect(() => {
    let { restaurant, currentLocation, Money, orderItems } = route.params;
    setOrderItems(orderItems);
    console.log("restaurant", restaurant); // thông tin quán ăn
    Geolocation.getCurrentPosition((info) => {
      console.log(info);
      setFromLocation(info.coords);
    });

    let fromLoc = currentLocation.gps;
    let toLoc = restaurant.location;
    let street = currentLocation.streetName;
    console.log("fromLoc_fromLoc", fromLoc);
    let mapRegion = {
      latitude: (fromLoc.latitude + toLoc.latitude) / 2,
      longitude: (fromLoc.longitude + toLoc.longitude) / 2,
      latitudeDelta: Math.abs(fromLoc.latitude - toLoc.latitude) * 2,
      longitudeDelta: Math.abs(fromLoc.longitude - toLoc.longitude) * 2,
    };
    console.log("mapRegion_mapRegion", mapRegion);
    setRestaurant(restaurant);
    setStreetName(street);
    setFromLocation(fromLoc);
    setToLocation(toLoc);
    setRegion(mapRegion);
    setTemporaryMoney(Money);
    setCurrentLocation(currentLocation);
  }, []);

  function calculateAngle(coordinates) {
    let startLat = coordinates[0]["latitude"];
    let startLng = coordinates[0]["longitude"];
    let endLat = coordinates[1]["latitude"];
    let endLng = coordinates[1]["longitude"];
    let dx = endLat - startLat;
    let dy = endLng - startLng;

    return (Math.atan2(dy, dx) * 180) / Math.PI;
  }
  const CallPhone = (number) => {
    let phoneNumber = "";
    if (Platform.OS === "android") {
      phoneNumber = `tel:${number}`;
    } else {
      phoneNumber = `telprompt:${number}`;
    }
    Linking.openURL(phoneNumber);
  };
  const CancelOrder = () => {
    Alert.alert(
      "Cản báo",
      "Bạn chắc chắn muốn hủy đơn hàng",
      [
        {
          text: "Thoát",
          // onPress: () => console.log('Cancel Pressed'),
          style: "Thoát",
        },
        {
          text: "Hủy",
          onPress: () => navigation.navigate("Home"),
          style: "Hủy",
        },
      ],
      { cancelable: false }
    );
  };

  function zoomIn() {
    let newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta / 2,
      longitudeDelta: region.longitudeDelta / 2,
    };

    setRegion(newRegion);
    mapView.current.animateToRegion(newRegion, 200);
  }

  function zoomOut() {
    let newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta * 2,
      longitudeDelta: region.longitudeDelta * 2,
    };

    setRegion(newRegion);
    mapView.current.animateToRegion(newRegion, 200);
  }

  function renderMap() {
    const destinationMarker = () => (
      <Marker coordinate={toLocation}>
        <View
          style={{
            height: 40,
            width: 40,
            borderRadius: 20,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: COLORS.white,
          }}
        >
          <View
            style={{
              height: 30,
              width: 30,
              borderRadius: 15,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: COLORS.primary,
            }}
          >
            <Image
              source={icons.pin}
              style={{
                width: 25,
                height: 25,
                tintColor: COLORS.white,
              }}
            />
          </View>
        </View>
      </Marker>
    );

    const carIcon = () => (
      <Marker
        coordinate={fromLocation}
        anchor={{ x: 0.5, y: 0.5 }}
        flat={true}
        rotation={angle}
      >
        <Image
          source={icons.car}
          style={{
            width: 40,
            height: 40,
          }}
        />
      </Marker>
    );
    const destination = { latitude: 20.9650352, longitude: 105.7825286 };
    // console.log('day_nhe',fromLocation,destination)
    return (
      <View style={{ flex: 1 }}>
        <MapView
          ref={mapView}
          provider={PROVIDER_GOOGLE}
          initialRegion={region}
          style={{ flex: 1 }}
        >
          <MapViewDirections
            origin={fromLocation}
            // destination={toLocation}
            destination={toLocation}
            apikey={GOOGLE_API_KEY}
            // apikey={'ssss'}
            strokeWidth={5}
            strokeColor={COLORS.primary}
            optimizeWaypoints={true}
            onReady={(result) => {
              setDuration(result.duration);
              console.log(`Distance: ${result.distance} km`);
              console.log(`Duration: ${result.duration} min.`);
              setDistance(result.distance);
              setTime(result.duration);
              if (!isReady) {
                // Fit route into maps
                mapView.current.fitToCoordinates(result.coordinates, {
                  edgePadding: {
                    right: SIZES.width / 20,
                    bottom: SIZES.height / 4,
                    left: SIZES.width / 20,
                    top: SIZES.height / 8,
                  },
                });

                // Reposition the car
                let nextLoc = {
                  latitude: result.coordinates[0]["latitude"],
                  longitude: result.coordinates[0]["longitude"],
                };

                if (result.coordinates.length >= 2) {
                  let angle = calculateAngle(result.coordinates);
                  setAngle(angle);
                }

                setFromLocation(nextLoc);
                setIsReady(true);
              }
            }}
          />
          {destinationMarker()}
          {carIcon()}
        </MapView>
      </View>
    );
  }
  const onButtonSuccess =()=>{
    // navigation.navigate("Home")
    try {
      const user = auth().currentUser;
      let { restaurant,orderItems } = route.params;
  
      console.log('restaurant_restaurant',restaurant,orderItems)
      let newPromise =  orderItems.map( async(item,index)=>{
            let body = {
              dish: item,
              email: user?.email,
              restaurant: restaurant,
              date:moment().format('DD/MM/YYYY')
            };
            let currentBank = JSON.parse(JSON.stringify(history));
            console.log("currentBank_currentBank", currentBank);
            console.log("currentBank_currentBank", [...currentBank, body]);
            console.log("body", body);
              await firestore()
                .collection("history")
                .add(body);
              dispatch(setDishHistoryOrder([...currentBank, body]));
    
      })
      Promise.all(newPromise) 
      Alert.alert("Thông báo", "Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi!", [
        {
          text: "OK",
          onPress: () => navigation.navigate("Home"),
          style: "OK",
        },
      ]);
    } catch (error) {
      console.log('error_error',error)
    }

    // console.log("body_body", body);
  }
  function renderDestinationHeader() {
    return (
      <View
        style={{
          position: "absolute",
          top: 12,
          left: 0,
          right: 0,
          height: 50,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            width: SIZES.width * 0.9,
            paddingVertical: SIZES.padding,
            paddingHorizontal: SIZES.padding * 2,
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.white,
          }}
        >
          <View
            style={{
              height: 25,
              width: 25,
              borderRadius: 15,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: COLORS.primary,
            }}
          >
            <Image
              source={icons.pin}
              style={{
                width: 20,
                height: 20,
                tintColor: COLORS.white,
              }}
            />
          </View>

          <View style={{ flex: 1, marginLeft: 14 }}>
            <Text style={{ color: COLORS.darkgray, ...FONTS.body4 }}>
              {restaurant?.name}
            </Text>
          </View>

          <Text style={{ ...FONTS.body3 }}>{Math.ceil(duration)} mins</Text>
        </View>
      </View>
    );
  }

  function renderDeliveryInfo() {
    return (
      <View
        style={{
          position: "absolute",
          bottom: 20,
          left: 0,
          right: 0,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            width: SIZES.width * 0.9,
            paddingVertical: SIZES.padding * 3,
            paddingHorizontal: SIZES.padding * 2,
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.white,
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            {/* Avatar */}
            <Image
              source={restaurant?.courier.avatar}
              style={{
                width: 50,
                height: 50,
                borderRadius: 25,
              }}
            />

            <View style={{ flex: 1, marginLeft: SIZES.padding }}>
              {/* Name & Rating */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                {/* <Text style={{ ...FONTS.h4 }}>{restaurant?.courier.name}</Text> */}
                <Text style={{ ...FONTS.h4, marginBottom: 5 }}>
                  Phương tiện: xe máy
                </Text>
              </View>

              {/* Restaurant */}
              {/* <Text style={{ color: COLORS.darkgray, ...FONTS.body4 }}>
                  {restaurant?.name}
                </Text> */}
            </View>
          </View>

          {/* Buttons */}
          <View
            style={{
              flexDirection: "row",
              marginTop: SIZES.padding * 2,
              justifyContent: "space-between",
              marginBottom: 16,
            }}
          >
            <TouchableOpacity
              style={{
                flex: 1,
                height: 50,
                marginRight: 10,
                backgroundColor: COLORS.primary,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              }}
              onPress={() => CallPhone(123456789)}
            >
              <Text style={{ ...FONTS.h4, color: COLORS.white }}>Gọi</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flex: 1,
                height: 50,
                backgroundColor: COLORS.secondary,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              }}
              onPress={() => CancelOrder()}
            >
              <Text style={{ ...FONTS.h4, color: COLORS.white }}>
                Hủy đơn hàng
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              flex: 1,
              height: 50,
              backgroundColor: COLORS.primary,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 10,
            }}
            onPress={onButtonSuccess}
          >
            <Text style={{ ...FONTS.h4, color: COLORS.white }}>Thành công</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderButtons() {
    return (
      <View
        style={{
          position: "absolute",
          bottom: SIZES.height * 0.4,
          right: SIZES.padding * 2,
          width: 50,
          height: 110,
          justifyContent: "space-between",
        }}
      >
        {/* Zoom In */}
        <TouchableOpacity
          style={{
            width: 50,
            height: 50,
            borderRadius: 25,
            backgroundColor: COLORS.white,
            alignItems: "center",
            justifyContent: "center",
            borderWidth: 0.5,
            borderColor: "gray",
          }}
          onPress={() => zoomIn()}
        >
          <Text style={{ ...FONTS.body1 }}>+</Text>
        </TouchableOpacity>

        {/* Zoom Out */}
        <TouchableOpacity
          style={{
            width: 50,
            height: 50,
            borderRadius: 25,
            backgroundColor: COLORS.white,
            alignItems: "center",
            justifyContent: "center",
            borderWidth: 0.5,
            borderColor: "gray",
          }}
          onPress={() => zoomOut()}
        >
          <Text style={{ ...FONTS.body1 }}>-</Text>
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      {renderMap()}
      {renderDestinationHeader()}
      {renderDeliveryInfo()}
      {renderButtons()}
    </View>
  );
};

export default CompleteFood;
