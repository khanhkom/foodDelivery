import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  left,
  right,
} from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import Geolocation from "@react-native-community/geolocation";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { COLORS, FONTS, icons, SIZES, GOOGLE_API_KEY } from "../constants";
import { TextInput } from "react-native-gesture-handler";
import { set } from "react-native-reanimated";
import disCode from "../Data/disCode";
const { width, height } = Dimensions.get("screen");

const CompleteOderDelivery = ({ route, navigation }) => {
  const mapView = React.useRef();

  const [restaurant, setRestaurant] = React.useState(null);
  const [streetName, setStreetName] = React.useState("");
  const [fromLocation, setFromLocation] = React.useState(null);
  const [toLocation, setToLocation] = React.useState(null);
  const [region, setRegion] = React.useState(null);
  const [currentLocation, setCurrentLocation] = React.useState(null);

  const [duration, setDuration] = React.useState(0);
  const [isReady, setIsReady] = React.useState(false);
  const [angle, setAngle] = React.useState(0);
  const [time, setTime] = useState(0);
  const [temporaryMoney, setTemporaryMoney] = React.useState(0);
  const [isPayments, setIsPayments] = React.useState(false);
  const [payments, setPayments] = React.useState(0);
  const [isDiscountCode, setIsDiscountCode] = React.useState(false);
  const [discountcode, setDiscountcode] = React.useState("chưa có mã");
  const [distance, setDistance] = useState(0);
  const [orderItems, setOrderItems] = useState([]);
  const [moneyShip, setMoneyShip] = useState(0);
  const [moneyShipV, setMoneyShipV] = useState(0);
  const [moneyDish, setMoneyDish] = useState(0);
  const [moneyDishV, setMoneyDishV] = useState(0);
  const [discode, setDiscode] = useState(disCode);
  React.useEffect(() => {
    console.log("discode", disCode);
    let {
      restaurant,
      currentLocation,
      Money,
      orderItems,
      MoneyShip,
      MoneyDish,
      Duration,
    } = route.params;
    console.log(Money);
    Geolocation.getCurrentPosition((info) => {
      console.log(info);
      setFromLocation(info.coords);
    });

    let fromLoc = currentLocation.gps;
    let toLoc = restaurant.location;
    let street = currentLocation.streetName;
    console.log("fromLoc_fromLoc", fromLoc);
    let mapRegion = {
      latitude: (fromLoc.latitude + toLoc.latitude) / 2,
      longitude: (fromLoc.longitude + toLoc.longitude) / 2,
      latitudeDelta: Math.abs(fromLoc.latitude - toLoc.latitude) * 2,
      longitudeDelta: Math.abs(fromLoc.longitude - toLoc.longitude) * 2,
    };
    console.log("mapRegion_mapRegion", mapRegion);
    setRestaurant(restaurant);
    setStreetName(street);
    setFromLocation(fromLoc);
    setToLocation(toLoc);
    setRegion(mapRegion);
    setTemporaryMoney(Money);
    setOrderItems(orderItems);
    setMoneyDish(MoneyDish);
    setMoneyDishV(MoneyDish);
    setMoneyShip(MoneyShip);
    setMoneyShipV(MoneyShip);
    setDiscode(disCode);
    setDuration(Duration);
    setCurrentLocation(currentLocation);
  }, []);
  const convertToNumberCommas = (val) => {
    if (val == null || val == undefined || val == "") return "";
    const num = Number(val)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    console.log(num);
    return num.toLocaleString();
  };
  function renderMenuFood() {
    return (
      <View
        style={{
          paddingTop: 12,
          paddingBottom: height / 3 + 24,
          marginHorizontal: 12,
          width: width - 24,
          flex: 1,
        }}
      >
        <ScrollView
          showsHorizontalScrollIndicator={true}
          showsVerticalScrollIndicator={false}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 12,
            }}
          >
            <View
              style={{
                padding: 12,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 30,
                borderWidth: 1,
                width: width / 1.5,
                backgroundColor: COLORS.primary,
              }}
            >
              <Text
                style={{ fontWeight: "bold", fontSize: 20, color: "white" }}
              >
                Tóm tắt thực đơn
              </Text>
            </View>
            <View
              style={{
                height: 1,
                width: width / 1.5,
                backgroundColor: "black",
                marginTop: 12,
              }}
            />
          </View>

          {orderItems.map((item, index) => {
            if (item.qty > 0) {
              return (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginVertical: 12,
                    paddingRight: 12,
                  }}
                >
                  <Image
                    source={item.itemId.photo}
                    resizeMode="cover"
                    style={{
                      width: 60,
                      height: 60,
                      borderRadius: 20,
                    }}
                  />
                  <View style={{ marginHorizontal: 12 }}>
                    <Text numberOfLines={2} style={{ fontSize: 18 }}>
                      {item.itemId.name}
                    </Text>
                    <Text>Số lượng: {item.qty}</Text>
                    <Text>
                      Giá tiền:{" "}
                      {convertToNumberCommas(
                        Number(item.qty) * Number(item.itemId.price)
                      )}{" "}
                      đ
                    </Text>
                  </View>
                </View>
              );
            }
          })}
          <TouchableOpacity onPress={() => navigation.navigate("Restaurant")}>
            <Text
              style={{
                color: COLORS.primary,
                textAlign: "right",
                fontstyle: "italic",
                fontSize: 14,
                textDecorationLine: "underline",
                right: 0,
              }}
            >
              Chỉnh sửa
            </Text>
          </TouchableOpacity>
          <View
            style={{
              height: 1,
              width: width / 1.5,
              backgroundColor: "black",
              marginLeft: (width - width / 1.5) / 2 - 12,
              marginVertical: 12,
            }}
          />

          <TouchableOpacity
            onPress={() => {
              setIsDiscountCode(true);
            }}
            style={{
              height: 40,
              borderColor: "black",
              borderWidth: 1,
              marginBottom: 12,
              borderRadius: 10,
              alignItems: "center",
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <View
              style={{ flexDirection: "row", alignItems: "center", width: 150 }}
            >
              <FontAwesome name="barcode" color="black" size={24} />
              <Text style={{ marginLeft: 12 }}>Mã giảm giá</Text>
            </View>

            <View
              style={{
                height: 32,
                alignItems: "center",
                width: 1,
                backgroundColor: "black",
              }}
            />

            <Text>{discountcode}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              setIsPayments(true);
            }}
            style={{
              height: 40,
              borderColor: "black",
              borderWidth: 1,
              marginBottom: 12,
              borderRadius: 10,
              alignItems: "center",
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                width: 150,
              }}
            >
              {/* <FontAwesome name="money" color="black" size={30} /> */}
              <Text numberOfLines={1}>Hình thức thanh toán</Text>
            </View>

            <View
              style={{
                height: 32,
                alignItems: "center",
                width: 1,
                backgroundColor: "black",
              }}
            />
            {payments == "0" ? <Text>Tiền mặt</Text> : null}
            {payments == "1" ? <Text>Thẻ thanh toán</Text> : null}
          </TouchableOpacity>

          <View
            style={{
              height: 1,
              width: width / 1.5,
              backgroundColor: "black",
              marginLeft: (width - width / 1.5) / 2 - 12,
              marginVertical: 12,
            }}
          />
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <Text>Phí giao hàng :</Text>
            {moneyShip != moneyShipV ? (
              <Text>
                {" "}
                {convertToNumberCommas(moneyShipV)} đ (
                {convertToNumberCommas(moneyShip)} đ)
              </Text>
            ) : (
              <Text>{convertToNumberCommas(moneyShip)} đ</Text>
            )}
          </View>
          <View
            style={{ flexDirection: "row", justifyContent: "space-around" }}
          >
            <Text>Tiền thực đơn :</Text>
            {moneyDish != moneyDishV ? (
              <Text>
                {" "}
                {convertToNumberCommas(moneyDishV)} đ (
                {convertToNumberCommas(moneyDish)} đ)
              </Text>
            ) : (
              <Text>{convertToNumberCommas(moneyDish)} đ</Text>
            )}
          </View>
          <View
            style={{
              height: 1,
              width: width / 1.5,
              backgroundColor: "gray",
              marginLeft: (width - width / 1.5) / 2 - 12,
              marginVertical: 12,
            }}
          />
          <View
            style={{ flexDirection: "row", justifyContent: "space-around" }}
          >
            <Text style={{ fontWeight: "bold", fontSize: 16 }}>
              Tổng số tiền :
            </Text>
            {moneyDishV + moneyShipV != temporaryMoney ? (
              <Text style={{ fontWeight: "bold" }}>
                {convertToNumberCommas(moneyDishV + moneyShipV)} đ (
                {convertToNumberCommas(temporaryMoney)} đ)
              </Text>
            ) : (
              <Text style={{ fontWeight: "bold" }}>
                {convertToNumberCommas(temporaryMoney)} đ
              </Text>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
  const calCodeDiscountV = (item) => {
    let code = item;
    let MoneyShip = Number(moneyShip);
    let MoneyDish = Number(moneyDish);
    let sotiengiamship = 0;
    let sotiengiamdish = 0;
    let MoneyShipV = Number(moneyShipV);
    let MoneyDishV = Number(moneyDishV);
    console.log("code", code);
    if (code.payments == null) {
      if (code.Condition != null) {
        if (MoneyDish >= Number(code.Condition)) {
          if (code.percent == true) {
            sotiengiamdish = (Number(code.value) / 100) * MoneyDish;
            sotiengiamship = (Number(code.value) / 100) * MoneyDish;
            if (code.shipMoney == true) {
              sotiengiamdish = 0;
            } else {
              sotiengiamship = 0;
            }
          } else {
            sotiengiamdish = Number(code.value);
            sotiengiamship = Number(code.value);
            if (code.shipMoney == true) {
              sotiengiamdish = 0;
            } else {
              sotiengiamship = 0;
            }
          }
        }
      } else {
        if (code.percent == true) {
          sotiengiamdish = (Number(code.value) / 100) * MoneyDish;
          sotiengiamship = (Number(code.value) / 100) * MoneyDish;
          if (code.shipMoney == true) {
            sotiengiamdish = 0;
          } else {
            sotiengiamship = 0;
          }
        } else {
          sotiengiamdish = Number(code.value);
          sotiengiamship = Number(code.value);
          if (code.shipMoney == true) {
            sotiengiamdish = 0;
          } else {
            sotiengiamship = 0;
          }
        }
      }
    } else if (code.payments == "1" && code.payments == payments) {
      if (code.Condition != null) {
        if (MoneyDish >= Number(code.Condition)) {
          if (code.percent == true) {
            sotiengiamdish = (Number(code.value) / 100) * MoneyDish;
            sotiengiamship = (Number(code.value) / 100) * MoneyDish;
            if (code.shipMoney == true) {
              sotiengiamdish = 0;
            } else {
              sotiengiamship = 0;
            }
          } else {
            sotiengiamdish = Number(code.value);
            sotiengiamship = Number(code.value);
            if (code.shipMoney == true) {
              sotiengiamdish = 0;
            } else {
              sotiengiamship = 0;
            }
          }
        }
      } else {
        if (code.percent == true) {
          sotiengiamdish = (Number(code.value) / 100) * MoneyDish;
          sotiengiamship = (Number(code.value) / 100) * MoneyDish;
          if (code.shipMoney == true) {
            sotiengiamdish = 0;
          } else {
            sotiengiamship = 0;
          }
        } else {
          sotiengiamdish = Number(code.value);
          sotiengiamship = Number(code.value);
          if (code.shipMoney == true) {
            sotiengiamdish = 0;
          } else {
            sotiengiamship = 0;
          }
        }
      }
    } else if (code.payments == "0" && code.payments == payments) {
      if (code.Condition != null) {
        if (MoneyDish >= Number(code.Condition)) {
          if (code.percent == true) {
            sotiengiamdish = (Number(code.value) / 100) * MoneyDish;
            sotiengiamship = (Number(code.value) / 100) * MoneyDish;
            if (code.shipMoney == true) {
              sotiengiamdish = 0;
            } else {
              sotiengiamship = 0;
            }
          } else {
            sotiengiamdish = Number(code.value);
            sotiengiamship = Number(code.value);
            if (code.shipMoney == true) {
              sotiengiamdish = 0;
            } else {
              sotiengiamship = 0;
            }
          }
        }
      } else {
        if (code.percent == true) {
          sotiengiamdish = (Number(code.value) / 100) * MoneyDish;
          sotiengiamship = (Number(code.value) / 100) * MoneyDish;
          if (code.shipMoney == true) {
            sotiengiamdish = 0;
          } else {
            sotiengiamship = 0;
          }
        } else {
          sotiengiamdish = Number(code.value);
          sotiengiamship = Number(code.value);
          if (code.shipMoney == true) {
            sotiengiamdish = 0;
          } else {
            sotiengiamship = 0;
          }
        }
      }
    }

    if (sotiengiamdish >= MoneyDish) {
      MoneyDishV = 0;
    } else {
      MoneyDishV = MoneyDish - sotiengiamdish;
    }
    if (sotiengiamship >= MoneyShip) {
      MoneyShipV = 0;
    } else {
      MoneyShipV = MoneyShip - sotiengiamship;
    }
    console.log("Money duoc giam", MoneyDishV, MoneyShip);
    setMoneyDishV(MoneyDishV);
    setMoneyShipV(MoneyShipV);
  };

  function renderDiscountCode() {
    if (isDiscountCode) {
      return (
        <View
          style={{
            position: "absolute",
            // bottom: 0,
            // left: 0,
            // right: 0,
            // alignItems: "center",
            // justifyContent: "center",
            flex: 1,
          }}
        >
          <ScrollView
            style={{
              width: width,
              height: height,
              paddingVertical: 12,
              paddingHorizontal: SIZES.padding * 2,
              borderRadiusLeftTop: SIZES.radius,
              backgroundColor: COLORS.white,
            }}
          >
            <View style={{ alignItems: "center" }}>
              <View
                style={{
                  flex: 1,
                  marginLeft: SIZES.padding,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text style={{ fontSize: 20 }}>Mã giảm giá</Text>
              </View>
            </View>

            <View
              style={{
                height: 1,
                width: width / 1.5,
                backgroundColor: "black",
                marginLeft: (width - width / 1.5) / 2 - 12,
                marginVertical: 12,
              }}
            />

            {/* discountcode */}

            <ScrollView
              style={{
                height: height - 220,
                marginVertical: 4,
                background: "black",
                borderWidth: 1,
                borderRadius: 20,
              }}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
            >
              {discode.map((item, index) => {
                return (
                  <View>
                    <TouchableOpacity
                      key={item}
                      onPress={() => {
                        if (discountcode == item.Title) {
                          setDiscountcode("chưa chọn mã");
                          setMoneyDishV(moneyDish);
                          setMoneyShipV(moneyShip);
                        } else {
                          setDiscountcode(item.Title);
                          calCodeDiscountV(item);
                        }

                        setIsDiscountCode(false);
                      }}
                      style={{
                        paddingHorizontal: 16,
                        // height: 50,
                        flexDirection: "row",
                        alignItems: "center",
                        marginRight: 16,
                        marginVertical: 6,
                      }}
                    >
                      <Image
                        source={icons.Discountcode}
                        style={{
                          width: 50,
                          height: 50,
                          // tintColor: COLORS.white,
                        }}
                      />
                      <View>
                        <Text
                          style={{
                            marginLeft: 12,
                            marginVertical: 3,
                            fontWeight: "bold",
                          }}
                          numberOfLines={1}
                        >
                          Mã : {item.Title}
                        </Text>
                        <Text
                          style={{ marginLeft: 12, marginVertical: 3 }}
                          numberOfLines={1}
                        >
                          Date :{item.ExpirationDate}
                        </Text>
                        <Text
                          style={{ marginHorizontal: 12, marginVertical: 3 }}
                        >
                          Thông tin chi tiết: {item.content}
                        </Text>
                        <View
                          style={{
                            padding: 6,
                            borderRadius: 10,
                            backgroundColor: COLORS.primary,
                            borderWidth: 1,
                            width: 100,
                            justifyContent: "center",
                            alignItems: "center",
                            marginTop: 6,
                          }}
                        >
                          {discountcode == item.Title ? (
                            <Text style={{ fontWeight: "bold" }}>Bỏ chọn</Text>
                          ) : (
                            <Text style={{ fontWeight: "bold" }}>Sử dụng</Text>
                          )}
                        </View>
                      </View>
                    </TouchableOpacity>
                    <View
                      style={{
                        height: 1,
                        width: width / 1.5,
                        backgroundColor: "black",
                        marginLeft: (width - width / 1.5) / 2 - 12,
                        marginVertical: 12,
                      }}
                    />
                  </View>
                );
              })}
            </ScrollView>

            {/* Buttons */}
            <View
              style={{
                flexDirection: "row",

                bottom: 0,
                justifyContent: "space-between",
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 1,
                  height: 50,
                  backgroundColor: COLORS.secondary,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 10,
                  marginBottom: 0,
                }}
                onPress={() => setIsDiscountCode(false)}
              >
                <Text style={{ ...FONTS.h4, color: COLORS.white }}>Trở về</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      );
    }
  }
  function renderPayments() {
    if (isPayments) {
      return (
        <View
          style={{
            position: "absolute",
            bottom: 0,
            left: 0,
            right: 0,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <ScrollView
            style={{
              width: width,
              height: height / 3,
              paddingVertical: 12,
              paddingHorizontal: SIZES.padding * 2,
              borderRadiusLeftTop: SIZES.radius,
              backgroundColor: COLORS.white,
            }}
          >
            <View style={{ alignItems: "center" }}>
              <View
                style={{
                  flex: 1,
                  marginLeft: SIZES.padding,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text style={{ fontSize: 20 }}>Hình thức thanh toán</Text>
              </View>
              <View
                style={{
                  height: 2,
                  backgroundColor: "black",
                  width: 200,
                  margin: 12,
                }}
              />
            </View>

            {/* payments */}

            <View>
              <TouchableOpacity
                onPress={() => {
                  setPayments(0);
                  setIsPayments(false);
                  setDiscountcode("chưa có mã");
                  setMoneyDishV(Number(moneyDish));
                  setMoneyShipV(Number(moneyShip));
                }}
                style={{
                  height: 40,
                  borderColor: "black",
                  borderWidth: 1,
                  marginVertical: 12,
                  borderRadius: 10,
                  backgroundColor: payments == "0" ? COLORS.primary : null,
                  alignItems: "center",
                  flexDirection: "row",
                  justifyContent: "space-around",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    width: 200,
                  }}
                >
                  <FontAwesome name="money" color="black" size={30} />
                  <Text style={{ marginLeft: 12 }}>Tiền mặt</Text>
                </View>
                {/* 
                <View
                  style={{
                    height: 32,
                    alignItems: "center",
                    width: 1,
                    backgroundColor: "black",
                  }}
                />

                <Text>{temporaryMoney} $</Text> */}
                <FontAwesome name="angle-right" color="black" size={30} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setPayments(1);
                  setIsPayments(false);
                  setDiscountcode("chưa có mã");
                  setMoneyDishV(Number(moneyDish));
                  setMoneyShipV(Number(moneyShip));
                }}
                style={{
                  height: 40,
                  borderColor: "black",
                  borderWidth: 1,
                  marginBottom: 12,
                  borderRadius: 10,
                  backgroundColor: payments == "1" ? COLORS.primary : null,
                  alignItems: "center",
                  flexDirection: "row",
                  justifyContent: "space-around",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    width: 200,
                  }}
                >
                  <FontAwesome name="bank" color="black" size={24} />
                  <Text style={{ marginLeft: 12 }}>Thẻ ngân hàng</Text>
                </View>
                <FontAwesome name="angle-right" color="black" size={30} />
              </TouchableOpacity>
            </View>

            {/* Buttons */}
            {/* <View
              style={{
                flexDirection: "row",
                position: "absolute",
                bottom: 0,
                bottom: 0,
                justifyContent: "space-between",
              }}
            > */}
            <TouchableOpacity
              style={{
                flex: 1,
                height: 50,
                backgroundColor: COLORS.secondary,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
                flex: 1,
              }}
              onPress={() => setIsPayments(false)}
            >
              <Text style={{ ...FONTS.h4, color: COLORS.white }}>Trở về</Text>
            </TouchableOpacity>
            {/* </View> */}
          </ScrollView>
        </View>
      );
    }
  }
  function renderDeliveryInfo() {
    return (
      <View
        style={{
          position: "absolute",
          bottom: 0,
          left: 0,
          right: 0,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <ScrollView
          style={{
            width: width,
            height: height / 3,
            paddingVertical: 12,
            paddingHorizontal: SIZES.padding * 2,
            borderRadiusLeftTop: SIZES.radius,
            backgroundColor: COLORS.white,
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            {/* Avatar */}
            {/* <Image
              source={restaurant?.courier.avatar}
              style={{
                width: 50,
                height: 50,
                borderRadius: 25,
              }}
            /> */}

            <View style={{ flex: 1, marginLeft: SIZES.padding }}>
              {/* Name & Rating */}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              />
              {/* <Text style={{ ...FONTS.body4 }}>
                Thời gian: {Math.ceil(Number(duration))} mins
              </Text> */}

              <Text style={{ ...FONTS.body4, marginVertical: 2 }}>
                Ghi chú :
              </Text>
              <TextInput
                style={{
                  ...FONTS.body4,
                  height: 40,
                  borderBottomWidth: 1,
                  borderColor: "black",
                }}
              />
            </View>
          </View>

          {/* payments */}

          <View>
            <View
              style={{
                height: 40,
                borderColor: "black",
                borderWidth: 1,
                marginVertical: 12,
                borderRadius: 10,
                alignItems: "center",
                flexDirection: "row",
                justifyContent: "space-around",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  width: 150,
                }}
              >
                <Text style={{ marginLeft: 12 }} numberOfLines={1}>
                  Số tiền thanh toán
                </Text>
              </View>

              <View
                style={{
                  height: 32,
                  alignItems: "center",
                  width: 1,
                  backgroundColor: "black",
                }}
              />

              {moneyDishV + moneyShipV != temporaryMoney ? (
                <Text style={{ fontWeight: "bold" }}>
                  {convertToNumberCommas(moneyDishV + moneyShipV)} đ
                </Text>
              ) : (
                <Text style={{ fontWeight: "bold" }}>
                  {convertToNumberCommas(temporaryMoney)} đ
                </Text>
              )}
            </View>
          </View>

          {/* Buttons */}
          <View
            style={{
              flexDirection: "row",

              bottom: 0,
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity
              style={{
                flex: 1,
                height: 50,
                marginRight: 10,
                backgroundColor: COLORS.primary,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              }}
              onPress={() =>
                navigation.navigate("CompleteFood", {
                  restaurant: restaurant,
                  currentLocation: currentLocation,
                  orderItems:orderItems
                  
                })
              }
            >
              <Text style={{ ...FONTS.h4, color: COLORS.white }}>Đặt hàng</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flex: 1,
                height: 50,
                backgroundColor: COLORS.secondary,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              }}
              onPress={() => navigation.goBack()}
            >
              <Text style={{ ...FONTS.h4, color: COLORS.white }}>Trở về</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      {renderMenuFood()}
      {renderDeliveryInfo()}
      {renderPayments()}
      {renderDiscountCode()}
    </View>
  );
};

export default CompleteOderDelivery;
