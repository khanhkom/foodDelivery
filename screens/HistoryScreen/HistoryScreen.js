import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  Alert,
} from "react-native";

import {
  icons,
  images,
  SIZES,
  COLORS,
  FONTS,
  GOOGLE_API_KEY,
} from "../../constants";
import restaurantData from "../../Data/restaurantData";
import categoryData from "../../Data/categoryData";
import moment from 'moment'
const { width, height } = Dimensions.get("window");
import { useDispatch, useSelector } from "react-redux";
import firestore from "@react-native-firebase/firestore";
import {setDishHistoryOrder} from '../../src/actions/dish';
import auth from "@react-native-firebase/auth";
import AsyncStorage from "@react-native-community/async-storage";
const initialCurrentLocation = {
  streetName: "Vị trí",
  gps: {
    latitude: 10.777289858224274,
    longitude: 106.69541231443705,
  },
};
const HistoryScreen = ({ navigation }) => {
  const [currentLocation, setCurrentLocation] = useState(
    initialCurrentLocation
  );
  const [userInformation,setUserInfor] = useState({

  })
  const user = useSelector(state=>state.user)
  const history = useSelector((state) => state.history);

  const dispatch = useDispatch()
  useEffect(()=>{
    async function getUser(){
      let Account = await AsyncStorage.getItem("@Account");
      console.log('Account',Account)
      if(Account&&Account!==null){
        setUserInfor(JSON.parse(Account))
        }else{
      setUserInfor({})
        }
    }
    getUser()
  },[user])
  useEffect(() => {
    const users = auth().currentUser;
    console.log('user_user',user?.email)
    const getBankByUser = async (user) => {
      const banks = await firestore()
        .collection("history")
        .get();
        console.log('history_order',banks.docs)
      let allOrderHistory = banks.docs.map((doc) => {
        return {
          idne: doc.id,
          ...doc.data(),
        };
      });
      console.log('allOrderHistory_allOrderHistory',allOrderHistory)

      let currentOrder = allOrderHistory.filter((element) => {
        return element.email === user?.email;
      });
      console.log('currentBank_currentBank',currentOrder);
    // console.log('user_user',user);
    if (currentOrder && currentOrder !== null) {
      dispatch(setDishHistoryOrder(currentOrder))
    }
  }
  getBankByUser(users)
  }, [user]);
  function renderHeader() {
    return (
      <View style={{ flexDirection: "row", height: 50, marginTop: 24 }}>
        <TouchableOpacity
          style={{
            width: 50,
            paddingLeft: SIZES.padding * 2,
            justifyContent: "center",
          }}
          onPress={() => navigation.goBack()}
        >
          <Image
            source={icons.back}
            resizeMode="contain"
            style={{
              width: 30,
              height: 30,
            }}
          />
        </TouchableOpacity>

        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              width: "70%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: SIZES.radius,
              padding: 16,
            }}
          >
            <Text
              style={{ fontSize: 18, textAlign: "auto", fontWeight: "bold" }}
              numberOfLines={2}
            >
              Lịch sử đặt món
            </Text>
          </View>
        </View>
      </View>
    );
  }
  const handlechooseres = (restau) => {
    let id = 2; // id của quán
    let name = "Quán Burger"; // tên quán
    let count = 0;

    restaurantData.map((item, index) => {
      if (item.id == restau?.id && item.name ==restau?.name) {
        navigation.navigate("Restaurant", {
          item,
          currentLocation,
        });
      } else {
        count = count + 1;
      }
    });
    console.log(count);
    if (count == restaurantData.length) {
      Alert.alert(
        "Thông báo",
        "không tìm được quán",
        [
          {
            text: "Thoát",
          },
        ],
        { cancelable: false }
      );
    }
  };
  console.log('userInformation_userInformation',userInformation)
  // if(!userInformation?.username){
  //   return(
  //     <SafeAreaView style={styles.container}>
  //     {renderHeader()}
  //     <View style={{ margin: 20,marginTop:40 }}>
  //       <Text style={{ fontSize: 14, fontWeight: "normal",textAlign:'center' }}>Vui lòng đăng nhập để xem thông tin lịch sử đặt món</Text>
  //     </View>
  //     </SafeAreaView>
  //   )
  //     }
  const ItemOrder=({item})=>{
    return(
      <View
      style={{
        margin: 16,
        // height: 100,
        borderRadius: 10,
        borderWidth: 0.5,
        // justifyContent: "center",
        flexDirection: "row",
        paddingVertical:5,
        paddingHorizontal:10
      }}
    >
      <Image
        source={item?.dish?.itemId?.photo??require("../../assets/images/historyfood.png")}
        style={{ width: 55, height: 55,alignSelf:'center',borderRadius:10 }}
        resizeMode='stretch'
      />
      <View style={{ flex: 6 / 10, marginLeft: 12 }}>
        <Text
          numberOfLines={2}
          style={{ color: "black", fontSize: 14, fontWeight: "bold" }}
        >
       {item?.dish?.itemId?.name}
        </Text>
        <Text
          numberOfLines={2}
          style={{ color: "black", fontSize: 14 }}
        >
       {item?.restaurant?.name}
        </Text>
        <Text> số tiền: {item?.dish?.total} đ</Text>
        <TouchableOpacity
          onPress={() => handlechooseres(item?.restaurant)}
          style={{
            padding: 6,
            backgroundColor: COLORS.primary,
            borderRadius: 12,
            width: 100,
            justifyContent: "center",
            alignItems: "center",
            marginVertical: 6,
          }}
        >
          <Text style={{ color: "white" }}>Đặt lại</Text>
        </TouchableOpacity>
      </View>
      <View style={{ flex: 4 / 10, marginTop: 30, marginLeft: 12 }}>
        <Text> {item?.date?? moment().format('DD/MM/YYYY')}</Text>
      </View>
    </View>
    )
  }
  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}
      <FlatList
      data={history||[]}
      renderItem={({item,index})=>{
        return(
          <ItemOrder item={item} index={index} />
        )
      }}
      keyExtractor={(item,index)=>String(index)}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray4,
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1,
  },
});

export default HistoryScreen;
