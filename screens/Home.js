import React, { useEffect } from "react";
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
} from "react-native";

import {
  icons,
  images,
  SIZES,
  COLORS,
  FONTS,
  GOOGLE_API_KEY,
} from "../constants";
import restaurantData from "../Data/restaurantData";
import categoryData from "../Data/categoryData";
import Geolocation from "@react-native-community/geolocation";
import Geocoder from "react-native-geocoding";
import { ScrollView } from "react-native-gesture-handler";
const { width, height } = Dimensions.get("window");
const GOOGLE_API_KEY_LOCATION = "AIzaSyA66KwUrjxcFG5u0exynlJ45CrbrNe3hEc";
const Home = ({ navigation }) => {
  useEffect(() => {
    console.log("res", restaurantData, categoryData);
    Geolocation.getCurrentPosition((info) => {
      console.log(info);
      // setFromLocation(info.coords);
      Geocoder.init(GOOGLE_API_KEY_LOCATION);
      Geocoder.from(info.coords.latitude, info.coords.longitude)
        .then((json) => {
          console.log("AAAAAAAAAAAAAAAAA", json);
          var addressComponent = json.results[0].formatted_address;
          console.log(addressComponent);
          setStreetName(addressComponent);
        })
        .catch((error) => console.warn(error));
    });
  }, []);
  // Dummy Datas

  const initialCurrentLocation = {
    streetName: "Vị trí",
    gps: {
      latitude: 10.777289858224274,
      longitude: 106.69541231443705,
    },
  };
  // price rating

  const [categories, setCategories] = React.useState(categoryData);
  const [selectedCategory, setSelectedCategory] = React.useState(null);
  const [restaurants, setRestaurants] = React.useState(restaurantData);
  const [isTypesOfDishes, setIsTypesOfDishes] = React.useState(false);
  const [currentLocation, setCurrentLocation] = React.useState(
    initialCurrentLocation
  );
  const [streetName, setStreetName] = React.useState("");

  function onSelectCategory(category) {
    let isTypesOfDishes1 = isTypesOfDishes;
    console.log("iss", isTypesOfDishes1);
    //filter restaurant
    let restaurantList = restaurantData.filter((a) =>
      a.categories.includes(category.id)
    );
    // console.log('abc',restaurantData, restaurantList);
    // console.log('abcd', restaurantList);
    if (isTypesOfDishes1) {
      restaurantList = restaurantData;
    }
    setRestaurants(restaurantList);

    setSelectedCategory(category);
    if (isTypesOfDishes1) {
      setIsTypesOfDishes(false);
    } else setIsTypesOfDishes(true);
  }

  function getCategoryNameById(id) {
    let category = categories.filter((a) => a.id == id);

    if (category.length > 0) return category[0].name;

    return "";
  }

  function renderHeader() {
    return (
      <View style={{ flexDirection: "row", height: 50, marginTop: 12 }}>
        {/* <TouchableOpacity
          onPress={() => {
            navigation.navigate("Locations");
          }}
          style={{
            width: 50,
            paddingLeft: SIZES.padding * 2,
            justifyContent: "center",
          }}
        >
          <Image
            source={icons.nearby}
            resizeMode="contain"
            style={{
              width: 30,
              height: 30,
            }}
          />
        </TouchableOpacity> */}

        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              width: "85%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: SIZES.radius,
              padding: 16,
              marginLeft: 20,
            }}
          >
            <Text style={{ fontSize: 16, textAlign: "auto" }} numberOfLines={2}>
              {streetName}
            </Text>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate("HistoryScreen")}
          style={{
            width: 50,
            paddingRight: SIZES.padding * 2,
            justifyContent: "center",
          }}
        >
          <Image
            source={icons.basket}
            resizeMode="contain"
            style={{
              width: 30,
              height: 30,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }

  function renderMainCategories() {
    const renderItem = ({ item }) => {
      return (
        <TouchableOpacity
          style={{
            padding: SIZES.padding,
            paddingBottom: SIZES.padding * 2,
            backgroundColor:
              selectedCategory?.id == item.id && isTypesOfDishes === true
                ? COLORS.primary
                : COLORS.white,
            borderRadius: SIZES.radius,
            alignItems: "center",
            justifyContent: "center",
            marginRight: SIZES.padding,
            ...styles.shadow,
          }}
          onPress={() => onSelectCategory(item)}
        >
          <View
            style={{
              width: 50,
              height: 50,
              borderRadius: 25,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor:
                selectedCategory?.id == item.id && isTypesOfDishes === true
                  ? COLORS.white
                  : COLORS.lightGray,
            }}
          >
            <Image
              source={item.icon}
              resizeMode="contain"
              style={{
                width: 30,
                height: 30,
              }}
            />
          </View>

          <Text
            style={{
              marginTop: SIZES.padding,
              color:
                selectedCategory?.id == item.id && isTypesOfDishes === true
                  ? COLORS.white
                  : COLORS.black,
              ...FONTS.body5,
            }}
          >
            {item.name}
          </Text>
        </TouchableOpacity>
      );
    };

    return (
      <View style={{ padding: SIZES.padding * 2 }}>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Text
            style={{
              ...FONTS.h1,
            }}
          >
            Dành cho bạn
          </Text>

          {/* <Text style={{ ...FONTS.h1 }}>Categories</Text> */}
        </View>
        <FlatList
          data={categories}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => `${item.id}`}
          renderItem={renderItem}
          contentContainerStyle={{ paddingVertical: SIZES.padding * 2 }}
        />
      </View>
    );
  }
  function renderHorizontal() {
    const renderItem = ({ item }) => (
      <TouchableOpacity
        style={{
          width: width - 64,
          marginTop: 12,
          marginHorizontal: 32,
        }}
        onPress={() =>
          navigation.navigate("Restaurant", {
            item,
            currentLocation,
          })
        }
      >
        {/* Image */}
        <View
          style={{
            marginBottom: SIZES.padding,
          }}
        >
          <Image
            source={item.photo}
            resizeMode="cover"
            style={{
              width: "100%",
              height: 200,
              borderRadius: SIZES.radius,
            }}
          />
          {/* 
          <View
            style={{
              position: "absolute",
              bottom: 0,
              height: 50,
              width: SIZES.width * 0.3,
              backgroundColor: COLORS.white,
              borderTopRightRadius: SIZES.radius,
              borderBottomLeftRadius: SIZES.radius,
              alignItems: "center",
              justifyContent: "center",
              ...styles.shadow,
            }}
          >
            <Text style={{ ...FONTS.h4 }}>{item.duration}</Text>
          </View> */}
        </View>
      </TouchableOpacity>
    );

    return (
      <FlatList
        data={restaurants}
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        horizontal
        keyExtractor={(item) => `${item.id}`}
        renderItem={renderItem}
        // contentContainerStyle={{
        //   paddingHorizontal: SIZES.padding * 2,
        //   paddingBottom: 30,
        // }}
      />
    );
  }

  function renderRestaurantList() {
    const renderItem = ({ item }) => (
      <TouchableOpacity
        style={{ marginBottom: SIZES.padding * 2 }}
        onPress={() =>
          navigation.navigate("Restaurant", {
            item,
            currentLocation,
          })
        }
      >
        {/* Image */}
        <View
          style={{
            marginBottom: SIZES.padding,
          }}
        >
          <Image
            source={item.photo}
            resizeMode="cover"
            style={{
              width: "100%",
              height: 200,
              borderRadius: SIZES.radius,
            }}
          />

          <View
            style={{
              position: "absolute",
              bottom: 0,
              height: 50,
              width: SIZES.width * 0.3,
              backgroundColor: COLORS.white,
              borderTopRightRadius: SIZES.radius,
              borderBottomLeftRadius: SIZES.radius,
              alignItems: "center",
              justifyContent: "center",
              ...styles.shadow,
            }}
          >
            <Text style={{ ...FONTS.h4 }}>{item.duration}</Text>
          </View>
        </View>

        {/* Restaurant Info */}
        <Text style={{ ...FONTS.body2 }}>{item.name}</Text>

        <View
          style={{
            marginTop: SIZES.padding,
            flexDirection: "row",
          }}
        >
          {/* Rating */}
          <Image
            source={icons.star}
            style={{
              height: 20,
              width: 20,
              tintColor: "red",
              marginRight: 10,
            }}
          />
          {/* <Text style={{ ...FONTS.body3 }}>{item.rating}</Text> */}

          {/* Categories */}
          <View
            style={{
              flexDirection: "row",
              marginLeft: 10,
            }}
          >
            {item.categories.map((categoryId) => {
              return (
                <View style={{ flexDirection: "row" }} key={categoryId}>
                  <Text style={{ ...FONTS.body3 }}>
                    {getCategoryNameById(categoryId)}
                  </Text>
                  <Text style={{ ...FONTS.h3, color: COLORS.darkgray }}>
                    {" "}
                    .{" "}
                  </Text>
                </View>
              );
            })}

            {/* Price */}
            {[1, 2, 3].map((priceRating) => (
              <Text
                key={priceRating}
                style={{
                  ...FONTS.body3,
                  color:
                    priceRating <= item.priceRating
                      ? COLORS.black
                      : COLORS.darkgray,
                }}
              >
                $
              </Text>
            ))}
          </View>
        </View>
      </TouchableOpacity>
    );

    return (
      <FlatList
        data={restaurants}
        keyExtractor={(item) => `${item.id}`}
        renderItem={renderItem}
        contentContainerStyle={{
          paddingHorizontal: SIZES.padding * 2,
          paddingBottom: 30,
        }}
      />
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}
      <ScrollView>
        {renderHorizontal()}
        {renderMainCategories()}
        {renderRestaurantList()}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray4,
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1,
  },
});

export default Home;
