import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Button,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  ActivityIndicator,
  Image,
} from "react-native";
// import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInput, ScrollView } from "react-native-gesture-handler";
// import DateTimePicker from '@react-native-community/datetimepicker';
// import Title from './Title';
import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
const { width, height } = Dimensions.get("window");
import auth from "@react-native-firebase/auth";
import AsyncStorage from "@react-native-community/async-storage";
import { useTheme } from "@react-navigation/native";
import firestore from "@react-native-firebase/firestore";
import { useDispatch, useSelector } from "react-redux";
import { setUserInformation } from "../../src/actions/userInfor";
import { images, COLORS, SIZES } from "../../constants";

export default function Information(props) {
  const { colors } = useTheme();
  //
  const [mode, setMode] = useState("date");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const userInformation = useSelector((state) => state.user);

  const [loading, setLoading] = useState(false);
  const [userInfor, setUserInfor] = useState({});
  const [sdt, setSdt] = useState("");
  const [diaChi, setDiaChi] = useState("");

  const [dayValue, setdayValue] = useState(1);
  //
  const [modalVisible, setModalVisible] = useState(false);
  const dispatch = useDispatch();
  // useEffect(()=>{
  //     // console.log('userInformation_userInformation',userInformation)
  //     if(!userInformation?.email){
  //         props.navigation.replace('Personal');
  //     }
  // },[userInformation]);
  // useEffect(()=>{
  //     console.log('route_route',props)
  //     if(props?.navigation?.state?.params){
  //         props.navigation.navigate('Discover');
  //     }
  // },[props])
  useEffect(() => {
    const user = auth().currentUser;
    const getUser = async (user) => {
      const usersne = await firestore()
        .collection("users")
        .get();
      let allUser = usersne.docs.map((doc) => {
        return {
          idne: doc.id,
          ...doc.data(),
        };
      });
      let users = allUser.find((element) => {
        return element.email === user?.email;
      });
      // console.log('users_users',users);

      if (users) {
        setName(users?.fullname ?? "");
        setDiaChi(users?.username ?? "");
        setEmail(users?.email ?? "");
        setSdt(users?.phonenumber ?? "");
        setdayValue(users?.birthday ?? "");
        setUserInfor(users);
        dispatch(setUserInformation(users));
      }
    };
    // console.log('user_user',user);
    if (user && user !== null) {
      user?.providerData?.forEach((userInfo) => {
        setName(userInfo?.displayName ?? "");
        setDiaChi(userInfo?.photoURL ?? "");
        setEmail(userInfo?.email ?? "");
        getUser(userInfo);
        // console.log('User info for provider: ', userInfo);
      });
    }
  }, []);

  const logOut = async () => {
    props.navigation.replace("Login");
    await AsyncStorage.removeItem("@Account");
    dispatch(
      setUserInformation({
        birthday: "",
        email: null,
        password: "",
        favorite: [],
        fullname: null,
        id: "",
        phonenumber: "",
        savelist: "",
        username: "",
      })
    );
    auth()
      .signOut()
      .then(() => console.log("User signed out!"));
  };
  const onUpdate = async () => {
    setLoading(true);

    let body = {
      ...userInfor,
      birthday: dayValue,
      fullname: name,
      phonenumber: sdt,
      username: diaChi,
    };
    console.log("update_update", body);

    await firestore()
      .collection("users")
      .doc(userInfor?.idne ?? "")
      .update(body)
      .then((res) => {
        console.log("res_res", res);
        console.log("User updated!");
        Alert.alert("Thông báo!", "Cập nhật thông tin thành công!", [
          { text: "Okay" },
        ]);
      });
    // const update = {
    //     displayName: name,
    //     // photoURL: 'https://my-cdn.com/assets/user/123.png',
    //     phoneNumber: sdt,
    //     photoURL: diaChi
    // };
    // await auth()
    //     .currentUser.updateProfile(update)
    //     .then((res) => {
    // Alert.alert('Thông báo!', 'Cập nhật thông tin thành công!', [
    //     { text: 'Okay' }
    // ]);
    //     })
    //     .catch((err) => {
    //         Alert.alert('Thông báo!', 'Cập nhật thông tin thất bại!', [
    //             { text: 'Okay' }
    //         ]);
    //     });
    setLoading(false);
  };
  function renderHeader() {
    return (
      <View style={{ flexDirection: "row", height: 50, marginVertical: 30 }}>
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              width: "70%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: SIZES.radius,
              padding: 16,
              borderWidth: 0.5,
            }}
          >
            <Text
              style={{ fontSize: 20, textAlign: "auto", fontWeight: "bold" }}
              numberOfLines={2}
            >
              Thông tin cá nhân
            </Text>
          </View>
        </View>
      </View>
    );
  }

  return (
    <ScrollView
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      style={[styles.container, { backgroundColor: colors.background }]}
    >
      {loading && (
        <View
          style={{
            marginTop: 30,
            marginBottom: -80,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <ActivityIndicator color="#000" size="large" />
        </View>
      )}
      <View>
        {/* <Title text="THÔNG TIN CÁ NHÂN" icon="user" size={20} />
        <Text style={[styles.searchText, { color: colors.text }]}>
          Thông tin cá nhân
        </Text> */}
        {renderHeader()}
        <View
          style={[
            styles.group,
            {
              backgroundColor: colors.background,
              borderColor: colors.text,
            },
          ]}
        >
          <View
            transparent
            style={{
              backgroundColor: colors.background,
              borderBottomWidth: 2,
              height: 80,
              borderBottomColor: colors.text,

              flexDirection: "row",
              alignItems: "center",
            }}
            iosBarStyle={"light-content"}
          >
            <Image
              source={images.avatar}
              style={{
                marginLeft: 30,
                height: 60,
                width: 60,
                borderRadius: 30,
                borderWidth: 1,
                borderColor: "white",
                justifyContent: "center",
              }}
            />
            <Text
              style={{
                fontWeight: "bold",
                color: colors.text,
                fontSize: 20,
                marginHorizontal: 12,
              }}
            >
              {" "}
              {name}
            </Text>
          </View>
          <TouchableOpacity
            style={styles.openModal}
            // disabled
            onPress={() => {
              setModalVisible(true);
            }}
          >
            <View style={styles.groupEdit}>
              <Icon
                name="pencil"
                size={20}
                style={{ marginLeft: 5, color: colors.text }}
              />
              <Text style={[styles.modalText, { color: colors.text }]}>
                Sửa thông tin
              </Text>
            </View>
          </TouchableOpacity>
          <View style={styles.groupInput}>
            <Text style={[styles.titleInput, { color: colors.text }]}>
              Họ và tên:
            </Text>
            <TextInput
              style={[styles.nameInput, { color: colors.text }]}
              onChangeText={(text) => setName(text)}
              value={name}
              placeholder="Họ và tên"
              placeholderTextColor={colors.text}
            />
          </View>
          <View style={styles.groupInput}>
            <Text style={[styles.titleInput, { color: colors.text }]}>
              SĐT:
            </Text>
            <TextInput
              style={[styles.nameInput, { color: colors.text }]}
              onChangeText={setSdt}
              keyboardType="numeric"
              value={sdt}
              placeholder="Số điện thoại"
              placeholderTextColor={colors.text}
            />
          </View>
          <View style={styles.groupInput}>
            <Text style={[styles.titleInput, { color: colors.text }]}>
              Email:
            </Text>
            <TextInput
              style={[styles.nameInput, { color: colors.text }]}
              editable={false}
              value={email}
              placeholder="@.com"
              placeholderTextColor={colors.text}
            />
          </View>
          <View style={styles.groupInput}>
            <Text style={[styles.titleInput, { color: colors.text }]}>
              Địa chỉ:
            </Text>
            <TextInput
              style={[styles.nameInput, { color: colors.text }]}
              onChangeText={setDiaChi}
              value={diaChi}
              placeholder="Địa chỉ"
              placeholderTextColor={colors.text}
            />
          </View>
          <View style={styles.groupInput}>
            <Text style={[styles.titleInput, { color: colors.text }]}>
              Ngày sinh:
            </Text>

            <TextInput
              // style={styles.day}
              style={[styles.nameInput, { color: colors.text }]}
              placeholder="Ngày"
              keyboardType="number-pad"
              onChangeText={setdayValue}
              value={dayValue.toString()}
              // maxLength={2}
              placeholderTextColor={colors.text}
            />
          </View>
        </View>

        <TouchableOpacity
          style={[{ marginTop: 60 }]}
          onPress={onUpdate}
          activeOpacity={0.6}
        >
          <LinearGradient
            colors={[COLORS.primary, "#01ab9d"]}
            style={styles.signIn}
          >
            <Text
              style={[
                styles.textSign,
                {
                  color: "#fff",
                },
              ]}
            >
              Cập nhật
            </Text>
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={logOut}
          style={[
            styles.signIn,
            {
              borderColor: COLORS.primary,
              borderWidth: 1,
              marginVertical: 15,
            },
          ]}
        >
          <Text
            style={[
              styles.textSign,
              {
                color: COLORS.primary,
              },
            ]}
          >
            Đăng xuất
          </Text>
        </TouchableOpacity>
      </View>
      <View style={{ height: 100 }} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  day: {
    // backgroundColor:'red',
    width: 200,
  },
  textStyle: {
    color: "#fff",
  },
  centeredView: {
    marginTop: 100,
  },
  searchText: {
    fontWeight: "bold",
    fontSize: 30,
    marginLeft: 20,
    marginTop: 50,
    marginBottom: 20,
  },
  group: {
    borderWidth: 2,
    marginHorizontal: 20,
    borderTopEndRadius: 20,
    borderBottomLeftRadius: 20,
    paddingTop: 10,
    paddingVertical: 20,
    paddingHorizontal: 5,
  },
  groupInput: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
  },
  titleInput: {
    marginLeft: 10,
    // marginTop: 12,

    fontFamily: "Sans-serif",
    fontSize: 15,
    marginRight: 10,
  },
  signIn: {
    width: "90%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    alignSelf: "center",
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
  nameInput: {
    color: "#000",
    fontSize: 15,
    padding: 0,
    margin: 0,
  },
  groupCheckbox: {
    flexDirection: "row",
  },
  titleCheck: {
    marginTop: 4,
  },
  line: {
    marginTop: 14,
    fontSize: 14,
    marginLeft: 20,
    marginRight: 20,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 4,
    marginHorizontal: 10,
    paddingHorizontal: 20,
  },
  openModal: {
    padding: 10,
  },
  modalText: {
    // marginBottom: 15,
    marginLeft: 10,
  },
  groupbutton: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginVertical: 70,
  },
  dangxuatbtn: {
    borderRadius: 20,
    padding: 10,
    backgroundColor: "lightgreen",
  },
  doimatkhaubtn: {
    borderRadius: 20,
    padding: 10,
    backgroundColor: "lightgreen",
  },
  groupEdit: {
    flexDirection: "row",
  },
});
