import React, { useEffect, useRef } from "react";
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  TextInput,
  Keyboard,
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import { icons, images, SIZES, COLORS, FONTS } from "../constants";
import restaurantData from "../Data/restaurantData";
import categoryData from "../Data/categoryData";

const search = ({ navigation }) => {
  const initialCurrentLocation = {
    streetName: "Kuching",
    gps: {
      latitude: 20.9788691,
      longitude: 105.7906469,
    },
  };
  const [categories, setCategories] = React.useState(categoryData);
  const [selectedCategory, setSelectedCategory] = React.useState(null);
  const [restaurants, setRestaurants] = React.useState(restaurantData);
  const [isTypesOfDishes, setIsTypesOfDishes] = React.useState(false);
  const [currentLocation, setCurrentLocation] = React.useState(
    initialCurrentLocation
  );
  const [textinput, setTextinput] = React.useState("");
  const [menuSearch, setMenuSearch] = React.useState([]);
  const [term, setTerm] = React.useState("");
  let timeout = useRef(null);

  function onSearch(text) {
    setTerm(text);
  }

  useEffect(() => {
    console.log("res", restaurantData, categoryData);
    if (term && term.length >= 3) {
      if (timeout) clearTimeout(timeout);

      timeout = setTimeout(() => {
        const menu = restaurantData.filter(
          (menuname) =>
            menuname.name.toLowerCase().includes(term.toLowerCase().trim()) //
        );
        console.log("menu", restaurants);
        setRestaurants(menu);
      }, 300);
      console.log("menu", restaurants);
    }
  }, [term]);
  // Dummy Datas

  function onSelectCategory(category) {
    let isTypesOfDishes1 = isTypesOfDishes;
    console.log("iss", isTypesOfDishes1);
    //filter restaurant
    let restaurantList = restaurantData.filter((a) =>
      a.categories.includes(category.id)
    );
    // console.log('abc',restaurantData, restaurantList);
    // console.log('abcd', restaurantList);
    if (isTypesOfDishes1) {
      restaurantList = restaurantData;
    }
    setRestaurants(restaurantList);

    setSelectedCategory(category);
    if (isTypesOfDishes1) {
      setIsTypesOfDishes(false);
    } else setIsTypesOfDishes(true);
  }

  function getCategoryNameById(id) {
    let category = categories.filter((a) => a.id == id);

    if (category.length > 0) return category[0].name;

    return "";
  }

  function renderHeader() {
    return (
      <View style={{ flexDirection: "row", height: 50, marginTop: 40 }}>
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              width: "90%",
              height: "100%",
              backgroundColor: COLORS.lightGray3,
              alignItems: "center",
              justifyContent: "space-around",
              borderRadius: SIZES.radius,
              flexDirection: "row",
            }}
          >
            <FontAwesome name="search" color="black" size={24} />

            <TextInput
              style={{
                ...FONTS.h3,
                flex: 7 / 10,
                justifyContent: "center",
                // backgroundColor: 'white',
                color: "black",
                fontSize: 16,
              }}
              placeholder="Tìm kiếm..."
              placeholderTextColor="gray"
              onChangeText={(value) => setTextinput(value)}
              onEndEditing={() => {
                Keyboard.dismiss();
                onSearch(textinput);
              }}
            />
            <TouchableOpacity
              onPress={() => {
                onSearch(textinput);
                Keyboard.dismiss();
              }}
            >
              <Text style={{ fontSize: 16, color: "black" }}>Search</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  function renderMainCategories() {
    const renderItem = ({ item }) => {
      return (
        <TouchableOpacity
          key={item.id}
          style={{
            padding: SIZES.padding,
            paddingBottom: SIZES.padding * 2,
            backgroundColor:
              selectedCategory?.id == item.id && isTypesOfDishes === true
                ? COLORS.primary
                : COLORS.white,
            borderRadius: SIZES.radius,
            alignItems: "center",
            justifyContent: "center",
            marginRight: SIZES.padding,
            ...styles.shadow,
          }}
          onPress={() => onSelectCategory(item)}
        >
          <View
            style={{
              width: 50,
              height: 50,
              borderRadius: 25,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor:
                selectedCategory?.id == item.id && isTypesOfDishes === true
                  ? COLORS.white
                  : COLORS.lightGray,
            }}
          >
            <Image
              source={item.icon}
              resizeMode="contain"
              style={{
                width: 30,
                height: 30,
              }}
            />
          </View>

          <Text
            style={{
              marginTop: SIZES.padding,
              color:
                selectedCategory?.id == item.id && isTypesOfDishes === true
                  ? COLORS.white
                  : COLORS.black,
              ...FONTS.body5,
            }}
          >
            {item.name}
          </Text>
        </TouchableOpacity>
      );
    };

    return (
      <View style={{ padding: SIZES.padding * 2 }}>
        <View style={{ justifyContent: "center", alignItems: "center" }} />
        <FlatList
          data={categories}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => `${item.id}`}
          renderItem={renderItem}
          contentContainerStyle={{ paddingVertical: SIZES.padding * 2 }}
        />
      </View>
    );
  }

  function renderRestaurantList() {
    const renderItem = ({ item }) => (
      <TouchableOpacity
        style={{ marginBottom: SIZES.padding * 2 }}
        onPress={() =>
          navigation.navigate("Restaurant", {
            item: item,
            currentLocation: currentLocation,
          })
        }
      >
        {/* Image */}
        <View
          style={{
            marginBottom: SIZES.padding,
          }}
        >
          <Image
            source={item.photo}
            resizeMode="cover"
            style={{
              width: "100%",
              height: 200,
              borderRadius: SIZES.radius,
            }}
          />

          <View
            style={{
              position: "absolute",
              bottom: 0,
              height: 50,
              width: SIZES.width * 0.3,
              backgroundColor: COLORS.white,
              borderTopRightRadius: SIZES.radius,
              borderBottomLeftRadius: SIZES.radius,
              alignItems: "center",
              justifyContent: "center",
              ...styles.shadow,
            }}
          >
            <Text style={{ ...FONTS.h4 }}>{item.duration}</Text>
          </View>
        </View>

        {/* Restaurant Info */}
        <Text style={{ ...FONTS.body2 }}>{item.name}</Text>

        <View
          style={{
            marginTop: SIZES.padding,
            flexDirection: "row",
          }}
        >
          {/* Rating */}
          <Image
            source={icons.star}
            style={{
              height: 20,
              width: 20,
              tintColor: "red",
              marginRight: 10,
            }}
          />
          {/* <Text style={{ ...FONTS.body3 }}>{item.rating}</Text> */}

          {/* Categories */}
          <View
            style={{
              flexDirection: "row",
              marginLeft: 10,
            }}
          >
            {item.categories.map((categoryId) => {
              return (
                <View style={{ flexDirection: "row" }} key={categoryId}>
                  <Text style={{ ...FONTS.body3 }}>
                    {getCategoryNameById(categoryId)}
                  </Text>
                  <Text style={{ ...FONTS.h3, color: COLORS.darkgray }}>
                    {" "}
                    .{" "}
                  </Text>
                </View>
              );
            })}

            {/* Price */}
            {[1, 2, 3].map((priceRating) => (
              <Text
                key={priceRating}
                style={{
                  ...FONTS.body3,
                  color:
                    priceRating <= item.priceRating
                      ? COLORS.black
                      : COLORS.darkgray,
                }}
              >
                $
              </Text>
            ))}
          </View>
        </View>
      </TouchableOpacity>
    );

    return (
      <FlatList
        data={restaurants}
        keyExtractor={(item) => `${item.id}`}
        renderItem={renderItem}
        contentContainerStyle={{
          paddingHorizontal: SIZES.padding * 2,
          paddingBottom: 30,
        }}
      />
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      {renderHeader()}
      {renderMainCategories()}
      {renderRestaurantList()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray4,
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1,
  },
});

export default search;
