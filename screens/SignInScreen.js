import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  StyleSheet,
  StatusBar,
  Alert,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import * as Animatable from "react-native-animatable";
// import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
// import firestore from '@react-native-firebase/firestore';

// import auth from '@react-native-firebase/auth';

// import { AuthContext } from './context';
// import AsyncStorage from '@react-native-community/async-storage';

const SignInScreen = ({ navigation }) => {
  const [data, setData] = React.useState({
    username: "user1",
    password: "password",
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
  });
  const [loading, setLoading] = useState(false);
  const [firstLoading, setFirstLoading] = useState(false);

  // const { signIn } = React.useContext(AuthContext);

  // useEffect(() => {
  //     async function getData() {
  //         setFirstLoading(true);
  //         let Account = await AsyncStorage.getItem('@Account');
  //         console.log('Account_Account', Account);
  //         if (Account && Account !== null) {
  //             setFirstLoading(false);
  //             navigation.navigate('Info');
  //         } else {
  //             setFirstLoading(false);
  //         }
  //     }
  //     getData();
  // }, []);
  // useEffect(()=>{
  //     const getUser = async()=>{
  //         const users = await firestore()
  //                             .collection('users')
  //                             .get();
  //                             let allUser  = users.docs.map(doc => doc.data());
  //                             // console.log('users_users',allUser)
  //     }
  //     getUser()
  // },[]);

  const isEmail = (value) => {
    // eslint-disable-next-line no-useless-escape
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !!regex.test(String(value).toLowerCase());
  };
  const textInputChange = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        username: val,
        check_textInputChange: isEmail(val),
        isValidUser: isEmail(val),
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: isEmail(val),
        isValidUser: isEmail(val),
      });
    }
  };

  const handlePasswordChange = (val) => {
    if (val.trim().length >= 6) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const handleValidUser = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        isValidUser: isEmail(val),
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
      });
    }
  };

  // const loginHandle = async () => {
  //     // navigation.navigate('SignUpScreen')
  //     const { username, password } = data;
  //     if (data.username.length === 0 || data.password.length === 0) {
  //         Alert.alert(
  //             'Wrong Input!',
  //             'Username or password field cannot be empty.',
  //             [{ text: 'Okay' }]
  //         );
  //         return;
  //     } else {
  //         setLoading(true);
  //         await auth()
  //             .signInWithEmailAndPassword(username, password)
  //             .then(async function (result) {
  //                 Alert.alert('Thông báo!', 'Đăng nhập thành công', [
  //                     { text: 'Okay' }
  //                 ]);
  //                 let body = {
  //                     username: username,
  //                     password: password
  //                 };
  //                 await AsyncStorage.setItem(
  //                     '@Account',
  //                     JSON.stringify(body)
  //                 );
  //                 setLoading(false);

  //                 navigation.navigate('Info');
  //                 // result.user.tenantId should be ‘TENANT_PROJECT_ID’.
  //             })
  //             .catch(function (error) {
  //                 setLoading(false);

  //                 if (error.code === 'auth/invalid-email') {
  //                     Alert.alert(
  //                         'Thất bại!',
  //                         'Email bạn nhập không hợp lệ!',
  //                         [{ text: 'Okay' }]
  //                     );
  //                 } else {
  //                     if (error.code === 'auth/wrong-password') {
  //                         Alert.alert(
  //                             'Thông báo!',
  //                             'Thông tin email hoặc password không chính xác!',
  //                             [{ text: 'Okay' }]
  //                         );
  //                     } else {
  //                         Alert.alert(
  //                             'Thông báo!',
  //                             'Đăng nhập thất bại! Vui lòng thử lại!',
  //                             [{ text: 'Okay' }]
  //                         );
  //                     }
  //                 }
  //                 console.log(error);
  //             });
  //     }
  // };
  if (firstLoading)
    return (
      <View
        style={{
          marginTop: 30,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <ActivityIndicator color="#fff" size="large" />
      </View>
    );
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      {loading && (
        <View
          style={{
            marginTop: 30,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <ActivityIndicator color="#fff" size="large" />
        </View>
      )}
      <View style={styles.header}>
        <Text style={styles.text_header}> Food Delivery xin chào!</Text>
      </View>
      <Animatable.View
        animation="fadeInUpBig"
        style={[
          styles.footer,
          {
            backgroundColor: "#fff",
          },
        ]}
      >
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
          <Text
            style={[
              styles.text_footer,
              {
                color: "black",
              },
            ]}
          >
            Email đăng nhập
          </Text>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="black" size={20} />
            <TextInput
              placeholder="Email đăng nhập"
              placeholderTextColor="#666666"
              style={[
                styles.textInput,
                {
                  color: "black",
                },
              ]}
              autoCapitalize="none"
              onChangeText={(val) => textInputChange(val)}
              onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
            />
            {data.check_textInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather name="check-circle" color="green" size={20} />
              </Animatable.View>
            ) : null}
          </View>
          {data.isValidUser ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>Vui lòng nhập email hợp lệ</Text>
            </Animatable.View>
          )}

          <Text
            style={[
              styles.text_footer,
              {
                color: "black",
                marginTop: 35,
              },
            ]}
          >
            Mật khẩu
          </Text>
          <View style={styles.action}>
            <Feather name="lock" color="black" size={20} />
            <TextInput
              placeholder="Mật khẩu của bạn"
              placeholderTextColor="#666666"
              secureTextEntry={data.secureTextEntry ? true : false}
              style={[
                styles.textInput,
                {
                  color: "black",
                },
              ]}
              autoCapitalize="none"
              onChangeText={(val) => handlePasswordChange(val)}
            />
            <TouchableOpacity onPress={updateSecureTextEntry}>
              {data.secureTextEntry ? (
                <Feather name="eye-off" color="grey" size={20} />
              ) : (
                <Feather name="eye" color="grey" size={20} />
              )}
            </TouchableOpacity>
          </View>
          {data.isValidPassword ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>
                Mật khẩu phải có ít nhất 6 ký tự.
              </Text>
            </Animatable.View>
          )}

          {/* <TouchableOpacity>
          <Text style={{color: '#009387', marginTop: 15}}>
            Forgot password?
          </Text>
        </TouchableOpacity> */}
          <View style={styles.button}>
            {/* <TouchableOpacity
                            style={styles.signIn}
                            onPress={() => {
                                loginHandle();
                            }}
                        >
                            <LinearGradient
                                colors={['#1db853', '#01ab9d']}
                                style={styles.signIn}
                            >
                                <Text
                                    style={[
                                        styles.textSign,
                                        {
                                            color: '#fff'
                                        }
                                    ]}
                                >
                                    Đăng nhập
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity> */}
            <TouchableOpacity
              style={[styles.signIn, { backgroundColor: "#08d4c4" }]}
              onPress={() => {
                loginHandle(data.username, data.password);
              }}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: "white",
                  },
                ]}
              >
                Đăng nhập
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                navigation.navigate("SignUpScreen");
              }}
              style={[
                styles.signIn,
                {
                  borderColor: "#08d4c4",
                  borderWidth: 1,
                  marginTop: 15,
                },
              ]}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: "#08d4c4",
                  },
                ]}
              >
                Đăng ký
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Animatable.View>
    </View>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#08d4c4",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 28,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
