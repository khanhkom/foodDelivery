import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Platform,
  StyleSheet,
  ScrollView,
  StatusBar,
  Alert,
} from "react-native";
import * as Animatable from "react-native-animatable";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import Fontisto from "react-native-vector-icons/Fontisto";
// import auth from '@react-native-firebase/auth';
// import firestore from '@react-native-firebase/firestore';

const SignInScreen = ({ navigation }) => {
  const [data, setData] = React.useState({
    username: "",
    password: "",
    confirm_password: "",
    check_textInputChange: false,
    secureTextEntry: true,
    confirm_secureTextEntry: true,
  });
  const [birthday, setbirthday] = useState(new Date().toISOString());
  const [sdt, setsdt] = useState("");
  const [name, setname] = useState("");
  const [usename, setusername] = useState("");
  // useEffect(()=>{
  //     const getUser = async()=>{
  //         const users = await firestore()
  //                             .collection('users')
  //                             .get();
  //                             let allUser  = users.docs.map(doc => doc.data());
  //                             // console.log('users_users',allUser)
  //     }
  //     getUser()
  // },[]);
  const textInputChange = (val) => {
    if (val.length !== 0) {
      setData({
        ...data,
        username: val,
        check_textInputChange: isEmail(val),
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: false,
      });
    }
  };

  const handlePasswordChange = (val) => {
    setData({
      ...data,
      password: val,
    });
  };

  const handleConfirmPasswordChange = (val) => {
    setData({
      ...data,
      confirm_password: val,
    });
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const updateConfirmSecureTextEntry = () => {
    setData({
      ...data,
      confirm_secureTextEntry: !data.confirm_secureTextEntry,
    });
  };
  const isEmail = (value) => {
    // eslint-disable-next-line no-useless-escape
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !!regex.test(String(value).toLowerCase());
  };
  // const InserUserToTable = async()=>{
  //     let body = {
  //         birthday:birthday,
  //         email:data.username,
  //         password:data.password,
  //         favorite:[],
  //         fullname:name,
  //         id:new Date().toISOString(),
  //         phonenumber:sdt,
  //         savelist:[],
  //         username:usename,
  //     }
  //   await  firestore().collection('users').add(body)
  // }
  // const onRegister = () => {
  //     if (data.username.length == 0 || data.password.length == 0) {
  //         Alert.alert('Thông báo', 'Vui lòng nhập đầy đủ thông tin', [
  //             { text: 'Okay' }
  //         ]);
  //     } else if (data.password !== data.confirm_password) {
  //         Alert.alert('Thông báo', 'Mật khẩu không trùng khớp!', [
  //             { text: 'Okay' }
  //         ]);
  //     } else if (!isEmail(data.username)) {
  //         Alert.alert('Thông báo', 'Vui lòng nhập đúng định dạng email', [
  //             { text: 'Okay' }
  //         ]);
  //     } else if (data.password.length < 6) {
  //         Alert.alert('Thông báo', 'Mật khẩu phải nhiều hơn 6 ký tự', [
  //             { text: 'Okay' }
  //         ]);
  //     } else {
  //         try {
  //             auth()
  //                 .createUserWithEmailAndPassword(
  //                     data.username,
  //                     data.password
  //                 )
  //                 .then(() => {
  //                     Alert.alert(
  //                         'Thành công!',
  //                         'Đăng ký thành công, vui lòng đăng nhập!',
  //                         [{ text: 'Okay' }]
  //                     );
  //                     InserUserToTable()
  //                     navigation.goBack();
  //                 })
  //                 .catch((error) => {
  //                     if (error.code === 'auth/email-already-in-use') {
  //                         Alert.alert('Thất bại!', 'Email đã được sử dụng!', [
  //                             { text: 'Okay' }
  //                         ]);
  //                     }
  //                     if (error.code === 'auth/invalid-email') {
  //                         Alert.alert(
  //                             'Thất bại!',
  //                             'Email bạn nhập không hợp lệ!',
  //                             [{ text: 'Okay' }]
  //                         );
  //                     }

  //                     console.error(error);
  //                 });
  //         } catch (error) {
  //             Alert.alert('Register fail!', error, [{ text: 'Okay' }]);
  //         }
  //     }
  // };
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>Đăng ký ngay nào!</Text>
      </View>
      <Animatable.View animation="fadeInUpBig" style={styles.footer}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
          <Text
            style={[
              styles.text_footer,
              {
                marginTop: 10,
              },
            ]}
          >
            Họ và tên
          </Text>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="#05375a" size={20} />
            <TextInput
              placeholder="Nhập họ tên"
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={(val) => setname(val)}
            />
            {data.check_textInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather name="check-circle" color="green" size={20} />
              </Animatable.View>
            ) : null}
          </View>
          <Text
            style={[
              styles.text_footer,
              {
                marginTop: 10,
              },
            ]}
          >
            Email
          </Text>
          <View style={styles.action}>
            <Fontisto name="email" color="#05375a" size={20} />
            <TextInput
              placeholder="Nhập email"
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={(val) => textInputChange(val)}
            />
            {data.check_textInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather name="check-circle" color="green" size={20} />
              </Animatable.View>
            ) : null}
          </View>

          <Text
            style={[
              styles.text_footer,
              {
                marginTop: 10,
              },
            ]}
          >
            Số điện thoại
          </Text>
          <View style={styles.action}>
            <Feather name="phone" color="#05375a" size={20} />
            <TextInput
              placeholder="Số điện thoại"
              keyboardType="numeric"
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={(val) => setsdt(val)}
            />
          </View>

          <Text
            style={[
              styles.text_footer,
              {
                marginTop: 10,
              },
            ]}
          >
            Mật khẩu
          </Text>
          <View style={styles.action}>
            <Feather name="lock" color="#05375a" size={20} />
            <TextInput
              placeholder="Mật khẩu của bạn"
              secureTextEntry={data.secureTextEntry ? true : false}
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={(val) => handlePasswordChange(val)}
            />
            <TouchableOpacity onPress={updateSecureTextEntry}>
              {data.secureTextEntry ? (
                <Feather name="eye-off" color="grey" size={20} />
              ) : (
                <Feather name="eye" color="grey" size={20} />
              )}
            </TouchableOpacity>
          </View>

          <Text
            style={[
              styles.text_footer,
              {
                marginTop: 10,
              },
            ]}
          >
            Nhập lại mật khẩu
          </Text>
          <View style={styles.action}>
            <Feather name="lock" color="#05375a" size={20} />
            <TextInput
              placeholder="Nhập lại mật khẩu"
              secureTextEntry={data.confirm_secureTextEntry ? true : false}
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={(val) => handleConfirmPasswordChange(val)}
            />
            <TouchableOpacity onPress={updateConfirmSecureTextEntry}>
              {data.confirm_secureTextEntry ? (
                <Feather name="eye-off" color="grey" size={20} />
              ) : (
                <Feather name="eye" color="grey" size={20} />
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.button}>
            {/* <TouchableOpacity
                            style={styles.signIn}
                            onPress={onRegister}
                        >
                            <LinearGradient
                                colors={['#1db853', '#01ab9d']}
                                style={styles.signIn}
                            >
                                <Text
                                    style={[
                                        styles.textSign,
                                        {
                                            color: '#fff'
                                        }
                                    ]}
                                >
                                    Đăng ký
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity> */}
            <TouchableOpacity
              style={[styles.signIn, { backgroundColor: "#08d4c4" }]}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: "white",
                  },
                ]}
              >
                Đăng ký
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={[
                styles.signIn,
                {
                  borderColor: "#08d4c4",
                  borderWidth: 1,
                  marginTop: 15,
                },
              ]}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: "#08d4c4",
                  },
                ]}
              >
                Đăng nhập
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Animatable.View>
    </View>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#08d4c4",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: Platform.OS === "ios" ? 3 : 5,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -4,
    paddingLeft: 10,
    color: "#05375a",
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
  textPrivate: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 20,
  },
  color_textPrivate: {
    color: "grey",
  },
});
