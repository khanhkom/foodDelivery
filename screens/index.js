import Home from "./Home";
import Restaurant from "./Restaurant";
import OrderDelivery from "./OrderDelivery";
import CompleteOrderDelivery from "./CompleteOrderDelivery";
import Search from "./Search";

export { Home, Search, Restaurant, OrderDelivery, CompleteOrderDelivery };
