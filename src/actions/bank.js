import { UPDATE_BANKS } from './types';

/*
 * Manually hiding the bottom navigation tab when media player bottom sheet is opened
 * Just to fix the android bottom tab not getting overlapped by media player bottom sheet smh
 */
export const setListBankConnected = (bank) => (dispatch) => {
    dispatch({ type: UPDATE_BANKS, payload: bank });
};