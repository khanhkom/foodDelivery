import { UPDATE_ORDER_HISTORY } from './types';

/*
 * Manually hiding the bottom navigation tab when media player bottom sheet is opened
 * Just to fix the android bottom tab not getting overlapped by media player bottom sheet smh
 */
export const setDishHistoryOrder = (dishs) => (dispatch) => {
    dispatch({ type: UPDATE_ORDER_HISTORY, payload: dishs });
};