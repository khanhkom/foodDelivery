import { UPDATE_USER,UPDATE_DOWNLOAD } from './types';

/*
 * Manually hiding the bottom navigation tab when media player bottom sheet is opened
 * Just to fix the android bottom tab not getting overlapped by media player bottom sheet smh
 */
export const setUserInformation = (user) => (dispatch) => {
    dispatch({ type: UPDATE_USER, payload: user });
};
export const setDownloadFile = (data) => (dispatch) => {
    dispatch({ type: UPDATE_DOWNLOAD, payload: data });
};
