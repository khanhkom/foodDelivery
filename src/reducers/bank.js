import { UPDATE_BANKS } from '../actions/types';

const INITIAL_STATE = [

]
   

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATE_BANKS:
      return action.payload;
    default:
      return state;
  }
}
