import { UPDATE_ORDER_HISTORY } from '../actions/types';

const INITIAL_STATE = [

]
   

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATE_ORDER_HISTORY:
      return action.payload;
    default:
      return state;
  }
}
