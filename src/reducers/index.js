import { combineReducers } from 'redux';
import user from './user';
import bank from './bank';
import history from './history';




export default combineReducers({
  user,
  bank,
  history
});
