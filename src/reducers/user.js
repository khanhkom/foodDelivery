import { UPDATE_USER } from '../actions/types';

const INITIAL_STATE = {
    birthday:'',
    email:null,
    password:'',
    favorite:[],
    fullname:null,
    id:'',
    phonenumber:'',
    savelist:'',
    username:'',
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATE_USER:
      return action.payload;
    default:
      return state;
  }
}
